### Libraries used in project

* [React](https://reactjs.org/)
* [React-router](https://github.com/ReactTraining/react-router) - routing library for React
* [Mobx](https://github.com/mobxjs/mobx) - Simple, scalable state management
* [Bootstrap 4](https://getbootstrap.com/)
* [Reactstrap](https://reactstrap.github.io/) - Bootstrap 4 React components
* [Formik](https://github.com/jaredpalmer/formik) - management forms in React
* [yup](https://github.com/jquense/yup) - validation library
* [Sass](https://sass-lang.com/) - css pre-processor
* [BEM](https://en.bem.info/) - methodology used for custom styles
* [react-datepicker](https://reactdatepicker.com/) - date picker component
* [react-dropzone](https://github.com/react-dropzone/react-dropzone) - file upload component
* [moment](https://momentjs.com/) - dates format library
* [superagent](https://visionmedia.github.io/superagent/) - light-weight progressive ajax API
* [cucumber](https://github.com/cucumber/cucumber-js) - test automation framework
* [selenium-webdriver](https://www.npmjs.com/package/selenium-webdriver) - driver that provides interfaces to interact with web elements

### Style guide

* **Fromating** IDE should support [prettier](https://github.com/prettier/prettier), prettier config is in root folder. Setup prettier formating on file save.
* Don't use functional components
* All React component props should described in propTypes
* declare propTypes after class like below

```
class InputField extends Component {
  // ....
}

InputField.propTypes = {
  id: PropTypes.string.isRequired,
  // ...
};
```

* if property is not required declare it in default props
* file structure

  * main folders
    * pages - main screens
    * components - React components and containers
    * atoms - small reusable components like `Input`
    * stores - mobx stores
  * component - folder names in Capital case. Inside js file with component names in Capital case and if necessary sass file with styles

### Run automation tests

* for running automation test start selenium server with command `yarn start-standalone`
* then at the command line type `yarn test-e2e`

### Run locally

* setup backend proxy in `package.json` to yours backend
* run script `yarn` to setup dependencies
* run script `yarn start` to run application

### Deploy

* run terminal command `yarn run build`. It creates build folder with production version
* copy build folder to yours server
