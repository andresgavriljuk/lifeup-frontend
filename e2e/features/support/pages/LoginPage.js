const {Builder, By, Key, until} = require('selenium-webdriver');
module.exports = class LoginPage {
  constructor(driver) {
    this.driver = driver;
    this.elements = {
      'Login Field': By.id('name'),
      'Password Field':By.id('password'),
      'Submit Button': By.id('subBtn'),
      'Role Widget': By.id('modalRoleSelectorBody'),
      'Employee': By.id('listActor1')
    }
  }
}