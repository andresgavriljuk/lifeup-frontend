const { By } = require('selenium-webdriver');
module.exports = class CreateVacationPage{
  constructor(driver) {
    this.driver = driver;
    this.elements = {
      'Vacation Form': By.id('vacationForm'),
      'Vacation type dropdown select': By.id('react-select-2--value'),
      'Põhipuhkus': By.xpath("//*[text()='Põhipuhkus']"),
      'Start Date': By.id('startDate'),
      'End Date': By.id('endDate'),
      'Submit': By.id('salvesta'),
      'Start Date Label': By.id('startDateLabel'),
      'End Date Label': By.id('endDateLabel'),
      'Succes Message': By.xpath("//*[text()='Puhkuseavaldus edukalt loodud']"),
      'Selected Vacation Type': By.id('selectedVacType')
    }
  }
};