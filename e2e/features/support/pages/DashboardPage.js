const { By } = require('selenium-webdriver');
module.exports = class DashboardPage{
  constructor(driver) {
    this.driver = driver;
    this.elements = {
      'Vacation Request Widget': By.id('myVacationRequestWidget'),
      'Add vacation button': By.id('toogleVac'),
      'Find Case modal Widget': By.id('findCaseModal'),
      'Create vacation button': By.id('kavita')
    }
  }
};