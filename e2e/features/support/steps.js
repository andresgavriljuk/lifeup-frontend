const {Given, When, Then} = require('cucumber');
const { expect } = require('chai');
const { until } = require('selenium-webdriver');

Given('I load an app', async function () {
    await this.driver.get('http://localhost:3000')
});

Then('At the {string} i type {string} into the {string}', async function (page, text, field) {
    await this.driver.findElement(this.page[page].elements[field]).sendKeys(text);

});

Then('At the {string} i click {string}', async function (page, element) {
    await this.driver.findElement(this.page[page].elements[element]).click();
});

Then('At the {string} element {string} is displayed', async function (page, element) {
  await this.driver.wait(until.elementLocated(this.page[page].elements[element]), 4000);
  await this.driver.findElement(this.page[page].elements[element]).isDisplayed();
});

Then('At the {string} {string} has value {string}', async function (page, element, value) {
  const elementText = await this.driver.findElement(this.page[page].elements[element]).getText();
  expect(elementText).to.equal(value);
});
