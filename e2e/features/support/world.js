const {setWorldConstructor} = require('cucumber');
const {Builder} = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');
require('chromedriver');
const LoginPage = require('./pages/LoginPage');
const DashboardPage = require('./pages/DashboardPage');
const CreateVacationPage = require('./pages/CreateVacationPage');

class CustomWorld {
  constructor() {
    this.driver = new Builder().forBrowser('chrome').build();
    this.loginPage = new LoginPage(this.driver);
    this.page = {
      'Login page': new LoginPage(this.driver),
      'Dashboard page': new DashboardPage(this.driver),
      'Create vacation page': new CreateVacationPage(this.driver),
    }
  }


  async getDriver() {

  }

  incrementBy(number) {
    this.variable += number
  }
}

setWorldConstructor(CustomWorld);