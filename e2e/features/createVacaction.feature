Feature: I want to create new vacation

  Scenario: create vacation scenario
    Given I load an app
    Then At the 'Login page' i type 'aleksander' into the 'Login Field'
    Then At the 'Login page' i type 'skafander' into the 'Password Field'
    And At the 'Login page' i click 'Submit Button'
    Then At the 'Login page' element 'Role Widget' is displayed
    And At the 'Login page' i click 'Employee'
    Then At the 'Dashboard page' element 'Vacation Request Widget' is displayed
    And At the 'Dashboard page' i click 'Add vacation button'
    Then At the 'Create vacation page' element 'Vacation Form' is displayed
    And At the 'Create vacation page' i click 'Vacation type dropdown select'
    And At the 'Create vacation page' i click 'Põhipuhkus'
    And At the 'Create vacation page' i click 'Start Date'
    Then At the 'Create vacation page' i type '08.05.2018' into the 'Start Date'
    And At the 'Create vacation page' i click 'End Date'
    Then At the 'Create vacation page' i type '24.05.2018' into the 'End Date'
    And At the 'Create vacation page' i click 'Submit'
    Then At the 'Create vacation page' element 'Succes Message' is displayed
    And At the 'Create vacation page' 'Start Date Label' has value '08.05.2018'
    And At the 'Create vacation page' 'End Date Label' has value '24.05.2018'
    And At the 'Create vacation page' 'Selected Vacation Type' has value 'Põhipuhkus'
