Feature: I want login to the app

  Scenario: Login scenario
    Given I load an app
    Then At the 'Login page' i type 'aleksander' into the 'Login Field'
    Then At the 'Login page' i type 'skafander' into the 'Password Field'
    And At the 'Login page' i click 'Submit Button'
    Then At the 'Login page' element 'Role Widget' is displayed
