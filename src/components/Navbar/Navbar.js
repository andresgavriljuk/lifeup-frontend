import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer, inject } from 'mobx-react';
import { Link } from 'react-router-dom';
import { Logo } from 'icons';
import { avatar } from 'imgs';
import {
  Navbar as BsNavbar,
  Nav,
  NavItem,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from 'reactstrap';
import { Dropdown } from 'atoms';
import { dashboardUrl, getUserUrl } from 'constants/links';
import { ROLES } from 'constants/classificators/translations';
import './navbar.css';

class Navbar extends Component {
  state = {
    rolesOpen: false,
    userOpen: false,
  };

  toggleRoles = () => {
    this.setState(prevState => ({
      rolesOpen: !prevState.rolesOpen,
    }));
  };

  toggleUser = () => {
    this.setState(prevState => ({
      userOpen: !prevState.userOpen,
    }));
  };

  handleChangeActor = id => {
    const { userStore, history } = this.props;

    userStore.selectActor(id);
    history.push(dashboardUrl);
  };

  render() {
    const { userStore, history } = this.props;

    return (
      <div className="appbar">
        <BsNavbar color="primary" dark expand="xs">
          <Link to={dashboardUrl}>
            <Logo />
          </Link>

          <Nav className="ml-auto" navbar>
            {userStore.actor && (
              <Dropdown
                isOpen={this.state.rolesOpen}
                toggle={this.toggleRoles}
                nav
                inNavbar
                className="active"
              >
                <DropdownToggle nav caret>
                  {ROLES[userStore.actor && userStore.actor.role]}
                </DropdownToggle>
                <DropdownMenu>
                  {userStore.actors.map(actor => (
                    <DropdownItem
                      key={actor.id}
                      onClick={() => this.handleChangeActor(actor.id)}
                    >
                      {ROLES[actor.role]}
                    </DropdownItem>
                  ))}
                </DropdownMenu>
              </Dropdown>
            )}
            {userStore.actor && (
              <React.Fragment>
                <Dropdown
                  isOpen={this.state.userOpen}
                  toggle={this.toggleUser}
                  nav
                  inNavbar
                  className="active border-left border-dark"
                >
                  <DropdownToggle nav caret>
                    {`${userStore.actor.firstName} ${userStore.actor.lastName}`}
                  </DropdownToggle>
                  <DropdownMenu>
                    <DropdownItem
                      onClick={() =>
                        history.push(getUserUrl(userStore.actor.id))
                      }
                    >
                      Minu andmed
                    </DropdownItem>
                    <DropdownItem onClick={userStore.logout}>
                      Logi välja
                    </DropdownItem>
                  </DropdownMenu>
                </Dropdown>
                <NavItem>
                  <img src={avatar} alt="avatar" width="39" height="39" />
                </NavItem>
              </React.Fragment>
            )}
          </Nav>
        </BsNavbar>
      </div>
    );
  }
}

Navbar.propTypes = {
  userStore: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default inject(({ userStore }) => ({ userStore }))(observer(Navbar));
