import React, { Component } from 'react';

import { observer } from 'mobx-react/index';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import SearchIcon from 'icons/SearchIcon';
import PropTypes from 'prop-types';
import {
  InputGroup,
  InputGroupAddon,
  Input,
  Row,
  Col,
  Container,
} from 'reactstrap';

import './findProcessModal.css';

class FindProcessModal extends Component {
  render() {
    const { toggle, modal } = this.props;
    return (
      <Modal isOpen={modal} toggle={toggle} size="lg">
        <ModalHeader toggle={toggle}>Vali juhtum</ModalHeader>
        <ModalBody>
          <br />
          <InputGroup className="modalInput">
            <Input className="searchInput" />
            <InputGroupAddon className="searchIcon" addonType="append">
              <SearchIcon />
            </InputGroupAddon>
          </InputGroup>
          <br />
          <Container>
            <Row>
              <Col>
                <div>Puhkuse korralduse loomine</div>
                <div className="vacationInfo">
                  Puhkuse avalduse loomine ja kooskõlastamine
                </div>
              </Col>
              <Row />
              <Button color="primary" xl={{ offset: 12 }}>
                KÄIVITA
              </Button>
            </Row>
          </Container>
        </ModalBody>
        <ModalFooter />
      </Modal>
    );
  }
}

FindProcessModal.propTypes = {
  toggle: PropTypes.func.isRequired,
  modal: PropTypes.bool.isRequired,
};

export default observer(FindProcessModal);
