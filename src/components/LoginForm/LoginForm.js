import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Formik, Field } from 'formik';
import { parseServerValidation } from 'utils/validators';
import { InputField, Button, FormControls } from 'atoms';
import yup from 'yup';
import './loginForm.css';

class LoginForm extends Component {
  render() {
    const { onLogin } = this.props;

    return (
      <Formik
        onSubmit={async (values, { setSubmitting, setErrors }) => {
          const result = await onLogin(values);
          if (result && result.errors) {
            setErrors(parseServerValidation(result.errors.validationErrors));
          }
          setSubmitting(false);
        }}
        initialValues={{ name: '', password: '' }}
        validationSchema={yup.object().shape({
          name: yup.string().required(),
          password: yup.string().required(),
        })}
        render={props => (
          <form
            className="login-form"
            onSubmit={props.handleSubmit}
            autoComplete="off"
          >
            <Field
              id="name"
              name="name"
              component={InputField}
              title="Kasutajanimi"
              placeholder="Sisestage kasutajanimi"
              required
            />
            <Field
              id="password"
              name="password"
              type="password"
              component={InputField}
              title="Parool"
              placeholder="Sisestage parool"
              required
            />
            <FormControls>
              <Button
                className="ml-auto"
                type="submit"
                color="primary"
                loading={props.isSubmitting}
                id="subBtn"
              >
                Sisene
              </Button>
            </FormControls>
          </form>
        )}
      />
    );
  }
}

LoginForm.propTypes = {
  onLogin: PropTypes.func.isRequired,
};

export default LoginForm;
