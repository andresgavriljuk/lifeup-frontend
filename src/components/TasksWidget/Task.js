import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import taskDescriptions from 'constants/taskDescriptions';
import { translateClassificatorValue } from 'utils/classificators';
import { parseServerDate } from 'utils/dates';
import { SERVER_DATE_FORMAT, DATE_FORMAT } from 'constants/dates';
import { withRouter } from 'react-router-dom';
import { getVacationViewUrl } from 'constants/links';
import classificators from 'constants/classificators';

class Task extends Component {
  render() {
    const { task, history } = this.props;

    return (
      <tr
        className="pointer"
        onClick={() =>
          history.push(getVacationViewUrl(task.process.vacation.id))
        }
      >
        <td className="pl-4">
          {taskDescriptions[task.type].title}
          <br />
          <span className="text-muted">
            {`Puhkuse avaldus - ${task.creator.firstName} ${
              task.creator.lastName
            } - ${translateClassificatorValue(
              classificators.VACATION_TYPE,
              task.process.vacation.type
            )} - ${parseServerDate(task.process.vacation.createdAt)}`}
          </span>
        </td>
        <td>
          {moment(task.createdAt, SERVER_DATE_FORMAT)
            .add(task.deadline, 'd')
            .format(DATE_FORMAT)}
        </td>
      </tr>
    );
  }
}

Task.propTypes = {
  task: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default withRouter(Task);
