import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Card, CardTitle, Table } from 'reactstrap';
import Task from './Task';
import { TASK_STATUS } from 'constants/classificators';
import { taskDateSorter } from 'utils/dates';
import { ROLES } from 'constants/classificators';
import './tasksWidget.css';

const titleByRole = {
  [ROLES.MANAGER]: (
    <span className="font-weight-600">
      Minu <span className="text-primary">ülesanded</span>
    </span>
  ),
  [ROLES.ACCOUNTANT]: (
    <span className="font-weight-600">
      Minu <span className="text-primary">ülesanded</span>
    </span>
  ),
};

class TasksWidget extends PureComponent {
  render() {
    const { tasks, className, role } = this.props;

    return (
      <Card className={className}>
        <CardTitle className="mx-4 mb-0 mt-2 pb-2 border-bottom ">
          {titleByRole[role]}
        </CardTitle>
        <div className="widget__body">
          {tasks.length > 0 ? (
            <Table hover className="task-table px-3">
              <thead>
                <tr>
                  <th className="border-0 w-75 pl-4">Kirjeldus</th>
                  <th className="border-0 w-25">Tähtaeg</th>
                </tr>
              </thead>
              <tbody>
                {tasks
                  .sort(taskDateSorter)
                  .filter(task => task.status === TASK_STATUS.CREATED)
                  .map(task => <Task key={task.id} task={task} />)}
              </tbody>
            </Table>
          ) : (
            <div className="text-muted text-center">Andmeid ei ole</div>
          )}
        </div>
      </Card>
    );
  }
}

TasksWidget.propTypes = {
  tasks: PropTypes.object.isRequired,
  className: PropTypes.string,
  role: PropTypes.string.isRequired,
};

export default TasksWidget;
