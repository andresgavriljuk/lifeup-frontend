import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { Row, Col } from 'reactstrap';
import './userInfo.css';

class UserInfo extends Component {
  render() {
    const { user } = this.props;

    return (
      <Row className="mx-2 mt-2 user-info">
        <Col md="12" lg="6">
          <dl className="row">
            <dt className="col-7">Struktuuriüksus:</dt>
            <dd className="col-5">{user.department}</dd>
            <dt className="col-7">Amet:</dt>
            <dd className="col-5">{user.position}</dd>
            <dt className="col-7">Sünniaasta:</dt>
            <dd className="col-5">1984</dd>
            <dt className="col-7">Sugu:</dt>
            <dd className="col-5">Mees</dd>
          </dl>
        </Col>
        <Col md="12" lg="6">
          <dl className="row">
            <dt className="col-7">Keel:</dt>
            <dd className="col-5">Eesti</dd>
            <dt className="col-7">Tööstaaž:</dt>
            <dd className="col-5">2 aastat</dd>
            <dt className="col-7">Eraparklasse liigipääs:</dt>
            <dd className="col-5">EI</dd>
            <dt className="col-7">Ligipääs x:</dt>
            <dd className="col-5">Jah</dd>
          </dl>
        </Col>
      </Row>
    );
  }
}

UserInfo.propTypes = {
  history: PropTypes.object.isRequired,
};

export default withRouter(UserInfo);
