import React, { Component } from 'react';
import { Card, CardTitle, ListGroup } from 'reactstrap';
import UserInfo from './UserInfo';
import PropTypes from 'prop-types';

class UserInformationWidget extends Component {
  render() {
    const { className } = this.props;

    return (
      <Card className={className}>
        <CardTitle className="mx-4 mb-0 mt-2 pb-2 border-bottom d-flex justify-content-between align-items-center">
          <span className="font-weight-600">
            Töötaja <span className="text-primary">andmed</span>
          </span>
        </CardTitle>
        <ListGroup>
          <UserInfo {...this.props} />
        </ListGroup>
      </Card>
    );
  }
}

UserInformationWidget.propTypes = {
  className: PropTypes.string,
};

export default UserInformationWidget;
