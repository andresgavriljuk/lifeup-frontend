import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Search, Button } from 'atoms';

class SearchProcess extends Component {
  render() {
    const { onProcessClick } = this.props;

    return (
      <React.Fragment>
        <Search />
        <div className="mt-4">
          <div className="d-flex justify-content-between">
            <div>
              <div>Puhkuse kooskõlastamine</div>
              <div className="text-muted">
                <small>Juhi ja raamatupidaja poolne kinnitamine</small>
              </div>
            </div>
            <Button
              color="primary"
              xl={{ offset: 12 }}
              onClick={onProcessClick}
              id='kavita'
            >
              KÄIVITA
            </Button>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

SearchProcess.propTypes = {
  onProcessClick: PropTypes.func.isRequired,
};

export default SearchProcess;
