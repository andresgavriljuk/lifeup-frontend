import React, { Component } from 'react';
import { Card, CardTitle, Table } from 'reactstrap';
import { Link } from 'react-router-dom';
import { newVacationUrl } from 'constants/links';
import VacationRequest from './VacationRequest';
import PlusIcon from 'icons/PlusIcon';
import { vacationDateSorter } from 'utils/dates';
import { PAGE_TYPE } from 'constants/classificators';
import PropTypes from 'prop-types';

const titleByPage = {
  [PAGE_TYPE.USER]: (
    <span className="font-weight-600">
      Töötaja puhkuse <span className="text-primary">korraldused</span>
    </span>
  ),
  [PAGE_TYPE.DASHBOARD]: (
    <span className="font-weight-600">
      Minu puhkuse <span className="text-primary">korraldused</span>
    </span>
  ),
};

class MyVacationRequestsWidget extends Component {
  render() {
    const { vacations, className, pageType } = this.props;

    return (
      <Card className={className} id="myVacationRequestWidget">
        <CardTitle className="mx-4 mb-0 mt-2 pb-2 border-bottom d-flex justify-content-between align-items-center">
          {titleByPage[pageType]}
          <Link to={newVacationUrl} id="toogleVac">
            <PlusIcon />
          </Link>
        </CardTitle>
        <div className="widget__body">
          {vacations.length > 0 ? (
            <Table hover className="px-3 request-table ">
              <tbody>
                {vacations
                  .sort(vacationDateSorter)
                  .map(vacation => (
                    <VacationRequest key={vacation.id} vacation={vacation} />
                  ))}
              </tbody>
            </Table>
          ) : (
            <div className="text-muted text-center">Andmeid ei ole</div>
          )}
        </div>
      </Card>
    );
  }
}
MyVacationRequestsWidget.propTypes = {
  vacations: PropTypes.object.isRequired,
  className: PropTypes.string,
  pageType: PropTypes.string.isRequired,
};

export default MyVacationRequestsWidget;
