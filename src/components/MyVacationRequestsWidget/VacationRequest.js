import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { translateClassificatorValue } from 'utils/classificators';
import classificators from 'constants/classificators';
import { getVacationViewUrl } from 'constants/links';
import './vacationRequest.css';

class VacationRequest extends Component {
  render() {
    const { vacation, history } = this.props;

    const vacationType = translateClassificatorValue(
      classificators.VACATION_TYPE,
      vacation.type
    );
    const { employee, startDate } = vacation;
    const status = translateClassificatorValue(
      classificators.VACATION_STATUS,
      vacation.status
    );

    return (
      <tr
        className="pointer"
        onClick={() => history.push(getVacationViewUrl(vacation.id))}
      >
        <td className="pl-4">
          <div className="vacationType">
            {`Puhkuse avaldus ${vacationType}`}
          </div>
          <div className="vacationInfo">
            {`Puhkuse avaldus ${employee.firstName} ${
              employee.lastName
            } - ${vacationType} - ${startDate}`}
          </div>
        </td>
        <td>
          <div className={`status status--${vacation.status}`}>{status}</div>
        </td>
      </tr>
    );
  }
}

VacationRequest.propTypes = {
  vacation: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default withRouter(VacationRequest);
