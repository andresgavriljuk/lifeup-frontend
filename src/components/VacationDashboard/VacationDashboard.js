import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer, inject } from 'mobx-react/index';
import { Container, Row, Col } from 'reactstrap';
import {
  MyVacationRequestsWidget,
  VacationInformationWidget,
} from 'components';
import { PAGE_TYPE } from 'constants/classificators';
import FindCaseModal from './FindCaseModal';
import './VacationDashboard.css';

class VacationDashboard extends Component {
  componentWillMount() {
    const { userStore, vacationStore } = this.props;

    vacationStore.getVacations(userStore.actor.id);
    vacationStore.getVacationData(userStore.actor.employeeId);
  }

  render() {
    const {
      vacationStore: { vacationData, vacations },
      dashboardStore,
    } = this.props;
    return (
      <Container>
        <FindCaseModal
          modal={dashboardStore.newProcessModalVisible}
          toggle={dashboardStore.toggleNewProcess}
        />
        <h2 className="mt-3 font-weight-600">Töötaja töölaud</h2>
        <Row className="widgetSection">
          <Col lg="8" md="7" xs="12">
            <MyVacationRequestsWidget
              className="widget mt-2"
              vacations={vacations}
              toggle={dashboardStore.toggleNewProcess}
              pageType={PAGE_TYPE.DASHBOARD}
            />
          </Col>
          <Col lg="4" md="5" xs="12">
            <VacationInformationWidget
              className="widget mt-2"
              vacationData={vacationData}
              pageType={PAGE_TYPE.DASHBOARD}
            />
          </Col>
        </Row>
      </Container>
    );
  }
}

VacationDashboard.propTypes = {
  vacationStore: PropTypes.object,
  dashboardStore: PropTypes.object,
  userStore: PropTypes.object,
};

export default inject('userStore', 'vacationStore', 'dashboardStore')(
  observer(VacationDashboard)
);
