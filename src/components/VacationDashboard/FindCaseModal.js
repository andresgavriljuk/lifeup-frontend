import React, { Component } from 'react';
import { observer } from 'mobx-react/index';
import { withRouter } from 'react-router-dom';
import { Button, Modal, ModalHeader, ModalBody } from 'reactstrap';
import { Search } from 'atoms';
import PropTypes from 'prop-types';
import { newVacationUrl } from 'constants/links';
import './VacationDashboard.css';

class FindCaseModal extends Component {
  render() {
    const { toggle, modal, history } = this.props;
    return (
      <Modal isOpen={modal} toggle={toggle} size="lg" centered id='findCaseModal'>
        <ModalHeader toggle={toggle}>Vali juhtum</ModalHeader>
        <ModalBody>
          <Search />
          <div className="mt-4">
            <div className="d-flex justify-content-between">
              <div>
                <div>Puhkuse korralduse loomine</div>
                <div className="vacationInfo">
                  Puhkuse avalduse loomine ja kooskõlastamine
                </div>
              </div>
              <Button
                color="primary"
                xl={{ offset: 12 }}
                onClick={() => history.push(newVacationUrl)}
                id='kavita'
              >
                KÄIVITA
              </Button>
            </div>
          </div>
        </ModalBody>
      </Modal>
    );
  }
}

FindCaseModal.propTypes = {
  toggle: PropTypes.func.isRequired,
  modal: PropTypes.bool.isRequired,
  history: PropTypes.object.isRequired,
};

export default withRouter(observer(FindCaseModal));
