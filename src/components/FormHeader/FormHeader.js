import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Document, Time, People } from 'icons';
import { VACATION_STATUS } from 'constants/classificators';
import { VACATION_STATUS as vacationStatusTranslations } from 'constants/classificators/translations';
import './formHeader.css';

class FormHeader extends PureComponent {
  handleProcessClick = e => {
    const { onProcessClick } = this.props;

    e.preventDefault();
    onProcessClick();
  };

  render() {
    const { header, status } = this.props;

    const statusClass = classNames('mb-1', `status--${status}`);

    return (
      <h5 className="vacation-header pl-3 pr-3 pb-2 pt-2">
        <div>
          <div className="form-header mb-1">{header}</div>
          <Document />
          <People className="ml-1" />
          <Time className="ml-1" />
        </div>
        {status && (
          <div>
            <div className={statusClass}>
              {vacationStatusTranslations[status]}
            </div>
            {status !== VACATION_STATUS.DRAFT && (
              <a
                className="vacation-header__link"
                href=""
                onClick={this.handleProcessClick}
              >
                Kuva Protsess
              </a>
            )}
          </div>
        )}
      </h5>
    );
  }
}

FormHeader.propTypes = {
  header: PropTypes.string.isRequired,
  isView: PropTypes.bool,
  status: PropTypes.string,
  onProcessClick: PropTypes.func,
};

FormHeader.defaultProps = {
  isView: false,
  status: '',
};

export default FormHeader;
