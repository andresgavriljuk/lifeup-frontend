import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer, inject } from 'mobx-react';
import { Link } from 'react-router-dom';
import _isEmpty from 'lodash/isEmpty';
import _get from 'lodash/get';
import {
  FormHeader,
  Bottombar,
  TaskNotification,
  Task,
  Timeline,
  SearchProcess,
} from 'components';
import { StaticField, FilesPreview, Button, NavLink } from 'atoms';
import { Nav, NavItem, Modal, ModalBody, ModalHeader } from 'reactstrap';
import { getVacationEditUrl } from 'constants/links';
import { parseServerDate } from 'utils/dates';
import classificators, {
  VACATION_TYPE,
  VACATION_STATUS,
  TASK_STATUS,
} from 'constants/classificators';

class VacationView extends Component {
  async componentWillMount() {
    const { id, vacationStore, taskStore, processStore } = this.props;

    await vacationStore.fetchVacation(id);
    taskStore.fetchVacationTasks(id);

    !_isEmpty(vacationStore.vacation.processes) &&
      processStore.fetchProcess(vacationStore.vacation.processes[0].id);
  }

  state = {
    taskVisible: true,
    timelineVisible: false,
    searchProcessVisible: false,
  };

  toggleTask = () => {
    this.setState(prevState => ({ taskVisible: !prevState.taskVisible }));
  };

  toggleSearchProcess = () => {
    this.setState(prevState => ({
      searchProcessVisible: !prevState.searchProcessVisible,
    }));
  };

  toggleTimeline = () => {
    this.setState(prevState => ({
      timelineVisible: !prevState.timelineVisible,
    }));
  };

  handleTask = async status => {
    const { processStore, id, vacationStore, taskStore } = this.props;

    processStore.runTask(
      status,
      vacationStore.vacation.processes[0].id,
      vacationStore.vacation.id
    );
    this.toggleTask();

    await vacationStore.fetchVacation(id);
    taskStore.fetchVacationTasks(id);

    !_isEmpty(vacationStore.vacation.processes) &&
      processStore.fetchProcess(vacationStore.vacation.processes[0].id);
  };

  render() {
    const { vacationStore: { vacation }, processStore, taskStore } = this.props;
    const { taskVisible } = this.state;

    const employeeFullName = `${_get(vacation, 'employee.firstName')} ${_get(
      vacation,
      'employee.lastName'
    )}`;

    const createdTasks = taskStore.vacationTasks.filter(
      t => t.status === TASK_STATUS.CREATED
    );

    return (
      <div className="container-fluid vacation-form-container">
        <FormHeader
          header={`Puhkuse avaldus - ${employeeFullName} - ${parseServerDate(
            vacation.createdAt
          )}`}
          status={vacation.status}
          onProcessClick={this.toggleTimeline}
        />
        <Nav pills>
          <NavItem>
            <NavLink href="#" active>
              Puhkuse avaldus
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink href="#" disabled>
              Teine sektsioon
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink href="#" disabled>
              Kolmas Sektsioon
            </NavLink>
          </NavItem>
        </Nav>
        <form className="vacation-form p-3 mb-4 bg-light position-relative">
          {createdTasks.length > 0 && (
            <React.Fragment>
              <TaskNotification
                value={createdTasks.length}
                onClick={this.toggleTask}
              />
              <Task
                task={createdTasks[0]}
                visible={taskVisible}
                onAction={this.handleTask}
              />
            </React.Fragment>
          )}
          <h5 className="text-primary font-weight-bold mt-3 mb-4">
            Töötaja andmed
          </h5>
          <div className="form-row">
            <div className="col-12 col-sm">
              <StaticField
                title="Nimi ja perekonnanimi"
                value={employeeFullName}
              />
            </div>
            <div className="col-12 col-sm">
              <StaticField
                title="Struktuuriüksus"
                value={_get(vacation, 'employee.department')}
              />
            </div>
          </div>
          <h5 className="text-primary font-weight-bold mt-3 mb-4">
            Puhkuse avalduse andmed
          </h5>
          <div className="form-row">
            <div className="col-12 col-sm">
              <StaticField
                title="Nimi ja perekonnanimi"
                value={vacation.type}
                id="selectedVacType"
                classificatorType={classificators.VACATION_TYPE}
              />
            </div>
            <div className="col-12 col-sm">
              <StaticField
                title="Esitamise kuupäev"
                value={vacation.createdAt}
                isDate
              />
            </div>
          </div>
          <div className="form-row">
            <div className="col-12 col-sm">
              <StaticField
                title="Puhkuse alguse kuupäev"
                id="startDateLabel"
                value={vacation.startDate}
                isDate
              />
            </div>
            <div className="col-12 col-sm">
              <StaticField
                title="Puhkuse lõppkuupäev"
                value={vacation.endDate}
                id="endDateLabel"
                isDate
              />
            </div>
          </div>
          <div className="form-row">
            <div className="col-12 col-sm-6">
              <StaticField title="Märkused" value={vacation.comment} />
            </div>
            {vacation.type === VACATION_TYPE.FATHER && (
              <div className="col-12 col-sm-6">
                <StaticField title="Ema isikukood" value={vacation.motherId} />
              </div>
            )}
            {vacation.type === VACATION_TYPE.REDUCING_WORKING_TIME && (
              <div className="col-12 col-sm-6">
                <StaticField
                  title="Põhjus"
                  value={vacation.reason}
                  classificatorType={classificators.VACATION_REASON}
                />
              </div>
            )}
          </div>
          {vacation.type === VACATION_TYPE.KID && (
            <div className="form-row">
              <div className="col-12 col-sm">
                <StaticField
                  title="Alla 14-aastat laste arv"
                  value={vacation.childrenFourteen}
                />
              </div>
              <div className="col-12 col-sm">
                <StaticField
                  title="Alla 3-aastat laste arv"
                  value={vacation.childrenThree}
                />
              </div>
            </div>
          )}
          {vacation.type === VACATION_TYPE.UNPAID_KID && (
            <div className="form-row">
              <div className="col-12 col-sm">
                <StaticField title="Lapse nimi" value={vacation.childName} />
              </div>
              <div className="col-12 col-sm">
                <StaticField
                  title="Lapse isikukood"
                  value={vacation.childCode}
                />
              </div>
            </div>
          )}
          <h5 className="text-primary font-weight-bold mt-3 mb-4">
            Lisa failid
          </h5>
          <FilesPreview className="mt-3 mb-4" files={vacation.files || []} />
          {vacation.status === VACATION_STATUS.DRAFT ? (
            <Bottombar>
              <Link to={getVacationEditUrl(vacation.id)}>
                <Button outline color="primary">
                  MUUDA
                </Button>
              </Link>
              <Button color="primary" onClick={this.toggleSearchProcess}>
                ALGATA PROTSESS
              </Button>
            </Bottombar>
          ) : (
            <Bottombar>
              <Button outline color="primary" disabled>
                UUS VERSIOON
              </Button>
              <Button outline color="primary" disabled>
                LISA KOMMENTAAR
              </Button>
              <Button outline color="primary" disabled>
                LISA ÜLESANNE
              </Button>
            </Bottombar>
          )}
        </form>
        {vacation.processes && (
          <Modal
            size="lg"
            isOpen={this.state.timelineVisible}
            toggle={this.toggleTimeline}
            centered
          >
            <ModalHeader toggle={this.toggleTimeline}>
              Puhkuse kooskõlastamine
            </ModalHeader>
            <ModalBody>
              <Timeline vacation={vacation} process={processStore.process} />
            </ModalBody>
          </Modal>
        )}
        {vacation.status === VACATION_STATUS.DRAFT && (
          <Modal
            size="lg"
            isOpen={this.state.searchProcessVisible}
            toggle={this.toggleSearchProcess}
            centered
          >
            <ModalHeader toggle={this.toggleSearchProcess}>
              Algata protsess
            </ModalHeader>
            <ModalBody>
              <SearchProcess
                onProcessClick={() => processStore.startProcess(vacation.id)}
              />
            </ModalBody>
          </Modal>
        )}
      </div>
    );
  }
}

VacationView.propTypes = {
  vacationStore: PropTypes.object.isRequired,
  processStore: PropTypes.object.isRequired,
  taskStore: PropTypes.object.isRequired,
  id: PropTypes.string.isRequired,
};

export default inject('vacationStore', 'processStore', 'taskStore')(
  observer(VacationView)
);
