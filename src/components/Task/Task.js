import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import classNames from 'classnames';
import { DATE_FORMAT, SERVER_DATE_FORMAT } from 'constants/dates';
import { Button, FormControls } from 'atoms';
import { TASK_STATUS } from 'constants/classificators';
import taskDescriptions from 'constants/taskDescriptions';
import { parseServerDate } from 'utils/dates';
import './task.css';

class Task extends Component {
  render() {
    const { task, visible, onAction } = this.props;
    const taskDescription = taskDescriptions[task.type];
    const taskClass = classNames('task pt-4 px-3', {
      'task--hidden': !visible,
    });

    return (
      <div className={taskClass}>
        <h5>{taskDescription && taskDescription.title}</h5>
        <p>{taskDescription && taskDescription.text}</p>
        <dl className="row">
          <dt className="col-3 text-muted">Algatatud:</dt>
          <dd className="col-9 text-muted">
            {parseServerDate(task.createdAt)}
          </dd>
          <dt className="col-3 text-muted">Tähtaeg:</dt>
          <dd className="col-9 text-muted">
            {moment(task.createdAt, SERVER_DATE_FORMAT)
              .add(task.deadline, 'd')
              .format(DATE_FORMAT)}
          </dd>
        </dl>
        <FormControls className="task__buttons">
          <Button
            color="success"
            onClick={() => onAction(TASK_STATUS.APPROVED)}
          >
            KOOSKÕLASTA
          </Button>
          <Button
            outline
            color="danger"
            onClick={() => onAction(TASK_STATUS.REJECTED)}
          >
            LÜKKA TAGASI
          </Button>
        </FormControls>
      </div>
    );
  }
}

Task.propTypes = {
  task: PropTypes.object.isRequired,
  visible: PropTypes.bool.isRequired,
  onAction: PropTypes.func.isRequired,
};

export default Task;
