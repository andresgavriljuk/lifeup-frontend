import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';

class PrivateRoute extends Component {
  render() {
    const { component: Component, user, actor, ...rest } = this.props;

    return (
      <Route
        {...rest}
        render={props =>
          user && actor ? (
            <Component {...props} {...rest} />
          ) : (
            <Redirect
              to={{
                pathname: '/login',
                state: { from: props.location },
              }}
            />
          )
        }
      />
    );
  }
}

PrivateRoute.propTypes = {
  user: PropTypes.object,
  actor: PropTypes.object,
};

PrivateRoute.defaultProps = {
  user: null,
  actor: null,
};

export default PrivateRoute;
