import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './taskNotification.css';

class TaskNotification extends Component {
  render() {
    const { value, onClick } = this.props;

    return (
      <React.Fragment>
        {value > 0 && (
          <div onClick={onClick} className="task-notification">
            {value}
          </div>
        )}
      </React.Fragment>
    );
  }
}

TaskNotification.propTypes = {
  value: PropTypes.number.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default TaskNotification;
