import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react/index';
import { Container, Row, Col } from 'reactstrap';
import { UserAvatar, UnknownAvatar } from 'imgs';
import { PAGE_TYPE } from 'constants/classificators';
import {
  MyVacationRequestsWidget,
  VacationInformationWidget,
  UserInformationWidget,
} from 'components';
import './UserPage.css';

class UserPage extends Component {
  render() {
    const { vacationStore: { vacationData, vacations }, employee } = this.props;

    return (
      <Container>
        <h2 className="mt-3 font-weight-600">{`${employee.firstName} ${
          employee.lastName
        }`}</h2>
        <Row>
          <Col lg="3" md="4" xs="3">
            <img
              src={employee.id === 1 ? UserAvatar : UnknownAvatar}
              alt="avatar"
              width="232"
              height="215"
              className="mt-2"
            />
          </Col>
          <Col lg="9" md="8" xs="12">
            <UserInformationWidget className="widget mt-2" user={employee} />
          </Col>
        </Row>
        <Row className="widgetSection">
          <Col lg="8" md="7" xs="12">
            <MyVacationRequestsWidget
              className="widget mt-2"
              vacations={vacations}
              pageType={PAGE_TYPE.USER}
            />
          </Col>
          <Col lg="4" md="5" xs="12">
            <VacationInformationWidget
              className="widget mt-2"
              vacationData={vacationData}
              pageType={PAGE_TYPE.USER}
            />
          </Col>
        </Row>
      </Container>
    );
  }
}

UserPage.propTypes = {
  vacationStore: PropTypes.object,
  employee: PropTypes.object.isRequired,
};

export default observer(UserPage);
