import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer, inject } from 'mobx-react';
import { Alert } from 'reactstrap';
import './alerts.css';

class Alerts extends Component {
  render() {
    const { alertStore } = this.props;
    return (
      <div className="alerts">
        {alertStore.alerts.map(alert => (
          <Alert
            key={alert.id}
            className="alerts__alert"
            color={alert.type}
            isOpen
            toggle={() => alertStore.removeAlert(alert)}
          >
            {alert.message}
          </Alert>
        ))}
      </div>
    );
  }
}

Alerts.propTypes = {
  alertStore: PropTypes.object.isRequired,
};

export default inject('alertStore')(observer(Alerts));
