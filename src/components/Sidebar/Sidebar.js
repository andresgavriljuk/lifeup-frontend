import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Home, NewCase } from 'icons';
import { observer, inject } from 'mobx-react';
import { dashboardUrl } from 'constants/links';
import { ROLES } from 'constants/classificators';
import './sidebar.css';

class Sidebar extends Component {
  render() {
    const { dashboardStore, userStore, match } = this.props;

    return (
      <div className="d-flex flex-column justify-content-between align-items-center sidebar py-2">
        <Link to={dashboardUrl}>
          <Home />
        </Link>
        {userStore.actor &&
          userStore.actor.role === ROLES.EMPLOYEE &&
          match.url === dashboardUrl && (
            <NewCase onClick={dashboardStore.toggleNewProcess} />
          )}
      </div>
    );
  }
}

Sidebar.propTypes = {
  dashboardStore: PropTypes.object.isRequired,
  userStore: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
};

export default inject('dashboardStore', 'userStore')(observer(Sidebar));
