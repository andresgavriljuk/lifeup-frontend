import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field } from 'formik';
import { FormHeader, Bottombar } from 'components';
import { Link } from 'react-router-dom';
import { dashboardUrl, getVacationViewUrl } from 'constants/links';
import {
  InputField,
  Button,
  SelectField,
  DateField,
  FilesPreview,
  FileUpload,
} from 'atoms';
import { classificatorAsSelect } from 'utils/classificators';
import classificators, { VACATION_TYPE } from 'constants/classificators';
import { VACATION_TYPE as VACATION_TYPE_TRANSLATIONS } from 'constants/classificators/translations';

class Form extends Component {
  state = { files: [] };

  onDrop = async uploadedFiles => {
    const { uploadFile, setFieldValue, values } = this.props;
    const fileData = await uploadFile(uploadedFiles[0]);

    setFieldValue('fileTokens', [...values.fileTokens, fileData.token]);
    this.setState(prevState => ({
      files: [
        ...prevState.files,
        { file: uploadedFiles[0], token: fileData.token },
      ],
    }));
  };

  render() {
    const { values, handleSubmit, errors } = this.props;
    const type = values.type;

    return (
      <React.Fragment>
        <FormHeader
          header={`Puhkuse avaldus - Aleksander Skafander ${
            type ? `- ${VACATION_TYPE_TRANSLATIONS[type]}` : ''
          } - ${values.createdAt}`}
        />
        <form
          className="vacation-form p-3 bg-light"
          onSubmit={handleSubmit}
          autoComplete="off"
          id="vacationForm"
        >
          <h5 className="text-primary font-weight-bold mt-3">Töötaja andmed</h5>
          <p className="text-secondary">Palun täitke alltoodud andmed</p>
          <div className="form-row">
            <div className="col-12 col-sm">
              <Field
                name="userName"
                type="text"
                component={InputField}
                id="selectedVacType"
                title="Nimi ja perekonnanimi"
                required
                disabled
              />
            </div>
            <div className="col-12 col-sm">
              <Field
                id="department"
                name="department"
                type="text"
                component={InputField}
                title="Struktuuriüksus"
                required
                disabled
              />
            </div>
          </div>
          <h5 className="text-primary font-weight-bold mt-3">
            Puhkuse avalduse andmed
          </h5>
          <p className="text-secondary">Palun täitke alltoodud andmed</p>
          <div className="form-row">
            <div className="col-12 col-sm">
              <Field
                id="type"
                name="type"
                component={SelectField}
                options={classificatorAsSelect(classificators.VACATION_TYPE)}
                title="Puhkuse liik"
                description="Teil on 18 kalendripäeva kasutamata põhipuhkust"
                required
              />
            </div>
            <div className="col-12 col-sm">
              <Field
                id="createdAt"
                name="createdAt"
                type="text"
                component={DateField}
                title="Esitamise kuupäev"
                required
                disabled
                isClearable={false}
              />
            </div>
          </div>
          <div className="form-row">
            <div className="col-12 col-sm">
              <Field
                id="startDate"
                name="startDate"
                component={DateField}
                title="Puhkuse alguse kuupäev"
                required
              />
            </div>
            <div className="col-12 col-sm">
              <Field
                id="endDate"
                name="endDate"
                component={DateField}
                title="Puhkuse lõppkuupäev"
                required
              />
            </div>
          </div>
          <div className="form-row">
            <div className="col-12 col-sm-6">
              <Field
                id="comment"
                name="comment"
                component={InputField}
                title="Märkused"
              />
            </div>
            {type === VACATION_TYPE.FATHER && (
              <div className="col-12 col-sm-6">
                <Field
                  id="motherId"
                  name="motherId"
                  type="number"
                  component={InputField}
                  title="Ema isikukood"
                  required
                />
              </div>
            )}
            {type === VACATION_TYPE.REDUCING_WORKING_TIME && (
              <div className="col-12 col-sm-6">
                <Field
                  id="reason"
                  name="reason"
                  component={SelectField}
                  options={classificatorAsSelect(
                    classificators.VACATION_REASON
                  )}
                  title="Põhjus"
                  required
                />
              </div>
            )}
          </div>
          {type === VACATION_TYPE.KID && (
            <div className="form-row">
              <div className="col-12 col-sm">
                <Field
                  id="children14"
                  name="children14"
                  type="number"
                  component={InputField}
                  title="Alla 14-aastat laste arv"
                  required
                />
              </div>
              <div className="col-12 col-sm">
                <Field
                  id="children3"
                  name="children3"
                  type="number"
                  component={InputField}
                  title="Alla 3-aastat laste arv"
                  required
                />
              </div>
            </div>
          )}
          {type === VACATION_TYPE.UNPAID_KID && (
            <div className="form-row">
              <div className="col-12 col-sm">
                <Field
                  id="childName"
                  name="childName"
                  component={InputField}
                  title="Lapse nimi"
                  required
                />
              </div>
              <div className="col-12 col-sm">
                <Field
                  id="childPersonalCode"
                  name="childPersonalCode"
                  type="number"
                  component={InputField}
                  title="Lapse isikukood"
                  required
                />
              </div>
            </div>
          )}
          <h5 className="text-primary font-weight-bold mt-3">Lisa failid</h5>
          <p className="text-secondary">
            <span className="text-danger">{errors.fileTokens}</span>
          </p>
          <FileUpload onDrop={this.onDrop} />
          <FilesPreview
            className="mt-3 mb-4"
            files={[...this.state.files, ...values.files]}
          />
          <Bottombar>
            <Link to={values.id ? getVacationViewUrl(values.id) : dashboardUrl}>
              <Button outline color="primary">
                TÜHISTA
              </Button>
            </Link>
            <Button type="submit" color="primary" id="salvesta">
              SALVESTA
            </Button>
          </Bottombar>
        </form>
      </React.Fragment>
    );
  }
}

Form.propTypes = {
  values: PropTypes.object.isRequired,
  uploadFile: PropTypes.func.isRequired,
  setFieldValue: PropTypes.func.isRequired,
};

export default Form;
