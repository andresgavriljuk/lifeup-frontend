import React, { Component } from 'react';
import PropTypes from 'prop-types';
import yup, { ref } from 'yup';
import { withRouter } from 'react-router-dom';
import moment from 'moment';
import { VACATION_TYPE } from 'constants/classificators';
import { observer, inject } from 'mobx-react';
import { Formik } from 'formik';
import { DATE_FORMAT, SERVER_DATE_FORMAT } from 'constants/dates';
import { parseServerValidation } from 'utils/validators';
import { getVacationViewUrl } from 'constants/links';
import Form from './Form';

import './vacationForm.css';

class VacationForm extends Component {
  componentWillMount() {
    const { id, vacationStore } = this.props;

    if (id) {
      vacationStore.fetchVacation(id);
    } else {
      vacationStore.resetVacation();
    }
  }

  mapPropsToValues = () => {
    const { userStore, vacationStore: { vacation } } = this.props;
    return {
      id: vacation.id,
      createdAt: vacation.createdAt
        ? moment(vacation.createdAt, SERVER_DATE_FORMAT)
        : moment(),
      userName: `${userStore.user.name} ${userStore.user.lastName}`,
      department: userStore.user.department,
      children14: vacation.childrenFourteen || 0,
      children3: vacation.childrenThree || 0,
      files: vacation.files || [],
      fileTokens: [],
      childCode: vacation.childCode || '',
      comment: vacation.comment || '',
      motherId: vacation.motherId || '',
      childName: vacation.childName || '',
      type: vacation.type || '',
      startDate: vacation.startDate
        ? moment(vacation.startDate, SERVER_DATE_FORMAT).format(
            SERVER_DATE_FORMAT
          )
        : null,
      endDate: vacation.endDate
        ? moment(vacation.endDate, SERVER_DATE_FORMAT).format(
            SERVER_DATE_FORMAT
          )
        : null,
    };
  };

  validationSchema = yup.object().shape({
    type: yup
      .string()
      .nullable()
      .required(),
    startDate: yup
      .date()
      .nullable()
      .when('type', {
        is: val => val === VACATION_TYPE.FATHER,
        then: yup.date().min(
          moment()
            .businessAdd(14, 'd')
            .toDate(),
          `Puhkuse alguse kuupäev peab olema suurem kui ${moment()
            .businessAdd(14, 'd')
            .format(DATE_FORMAT)}`
        ),
        otherwise: yup.date().min(
          moment()
            .businessAdd(10, 'd')
            .toDate(),
          `Puhkuse alguse kuupäev peab olema suurem kui ${moment()
            .businessAdd(10, 'd')
            .format(DATE_FORMAT)}`
        ),
      })
      .required(),
    endDate: yup
      .date()
      .nullable()
      .required()
      .isAfter(
        ref('startDate'),
        'Puhkuse lõpp kuupäev ei saa olla ennem kui algus kuupäev'
      )
      .when('type', {
        is: val => val === VACATION_TYPE.MAIN,
        then: yup.date().notFr('Puhkus ei saa lõppeda neljapäeval'),
      }),
    fileTokens: yup.array().when('type', {
      is: val =>
        [
          VACATION_TYPE.FATHER,
          VACATION_TYPE.PARENT,
          VACATION_TYPE.STUDENT,
        ].includes(val),
      then: yup.array().min(1, 'Vähemalt üks fail peab olema lisatud'),
    }),
    motherId: yup.string().when('type', {
      is: val => val === VACATION_TYPE.FATHER,
      then: yup
        .string()
        .nullable()
        .required(),
    }),
    children14: yup.number().when('type', {
      is: val => val === VACATION_TYPE.KID,
      then: yup
        .number()
        .nullable()
        .required(),
    }),
    children3: yup.number().when('type', {
      is: val => val === VACATION_TYPE.KID,
      then: yup
        .number()
        .nullable()
        .required(),
    }),
    reason: yup.string().when('type', {
      is: val => val === VACATION_TYPE.REDUCING_WORKING_TIME,
      then: yup
        .string()
        .nullable()
        .required(),
    }),
    childName: yup.string().when('type', {
      is: val => val === VACATION_TYPE.UNPAID_KID,
      then: yup
        .string()
        .nullable()
        .required(),
    }),
    childPersonalCode: yup.string().when('type', {
      is: val => val === VACATION_TYPE.UNPAID_KID,
      then: yup
        .string()
        .nullable()
        .required(),
    }),
  });

  render() {
    const { vacationStore, readonly, history } = this.props;

    return (
      <div className="container-fluid vacation-form-container">
        <Formik
          onSubmit={async (values, actions) => {
            const result = values.id
              ? await vacationStore.updateVacation(values)
              : await vacationStore.createVacation(values);

            if (result && result.errors) {
              actions.setErrors(
                parseServerValidation(result.errors.validationErrors)
              );
              actions.setSubmitting(false);
            } else {
              actions.setSubmitting(false);
              history.push(getVacationViewUrl(result));
            }
          }}
          validationSchema={this.validationSchema}
          initialValues={this.mapPropsToValues()}
          enableReinitialize
          render={props => (
            <Form
              {...props}
              uploadFile={vacationStore.uploadFile}
              readonly={readonly}
            />
          )}
        />
      </div>
    );
  }
}

VacationForm.propTypes = {
  vacationStore: PropTypes.object.isRequired,
  id: PropTypes.string,
  readonly: PropTypes.bool,
  history: PropTypes.object.isRequired,
};

VacationForm.defaultProps = {
  id: null,
  readonly: false,
};

export default withRouter(
  inject('vacationStore', 'userStore')(observer(VacationForm))
);
