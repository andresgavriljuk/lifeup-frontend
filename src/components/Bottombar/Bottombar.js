import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './bottombar.css';

class Bottombar extends Component {
  render() {
    const { children } = this.props;

    return <div className="bottombar">{children}</div>;
  }
}

Bottombar.propTypes = {
  children: PropTypes.node,
};

export default Bottombar;
