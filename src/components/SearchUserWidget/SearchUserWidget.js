import React, { Component } from 'react';
import { Card, CardTitle, ListGroup } from 'reactstrap';
import { observer } from 'mobx-react';
import { Search } from 'atoms';
import PropTypes from 'prop-types';
import UserItem from './UserItem';
import { ROLES } from 'constants/classificators';
import './SearchUserWidget.css';

const titleByRole = {
  [ROLES.MANAGER]: (
    <span className="font-weight-600">
      Minu struktuuriüksuse <span className="text-primary">töötajad</span>
    </span>
  ),
  [ROLES.ACCOUNTANT]: (
    <span className="font-weight-600">
      TTÜ <span className="text-primary">töötajad</span>
    </span>
  ),
};

class SearchUserWidget extends Component {
  state = { query: '' };

  handleChange = event => {
    this.setState({
      query: event.target.value.trim(),
    });
  };

  render() {
    const { className, users, role } = this.props;
    const { query } = this.state;

    const filtered = users.filter(user =>
      `${user.firstName}${user.lastName}`
        .toLowerCase()
        .includes(query.toLowerCase())
    );

    return (
      <Card className={className}>
        <CardTitle className="mx-4 mb-0 mt-2 pb-2 border-bottom d-flex justify-content-between align-items-center">
          {titleByRole[role]}
        </CardTitle>
        <Search className="mx-4 mb-1" onChange={this.handleChange} />
        <div className="widget__body">
          {filtered.length > 0 ? (
            <ListGroup>
              {filtered.map(user => <UserItem key={user.id} user={user} />)}
            </ListGroup>
          ) : (
            <div className="text-muted text-center">Andmeid ei ole</div>
          )}
        </div>
      </Card>
    );
  }
}
SearchUserWidget.propTypes = {
  users: PropTypes.object.isRequired,
  className: PropTypes.string,
};

export default observer(SearchUserWidget);
