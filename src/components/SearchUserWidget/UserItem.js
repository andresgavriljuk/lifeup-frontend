import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { ListGroupItem } from 'reactstrap';
import UserIcon from 'icons/User';
import { getUserUrl } from 'constants/links';
import './SearchUserWidget.css';

class UserItem extends Component {
  render() {
    const { user, history } = this.props;

    return (
      <ListGroupItem
        className="d-flex justify-content-between align-items-center border-0"
        action
        onClick={() => history.push(getUserUrl(user.id))}
      >
        <div>
          <div style={{ display: 'inline-block' }}>
            <UserIcon className="userIcon" />
          </div>
          <div style={{ display: 'inline-block', marginLeft: '15px' }}>
            <div className="userType">
              {`${user.firstName} ${user.lastName}`}
            </div>
            <div className="userInfo">{`${user.position}`}</div>
          </div>
        </div>
      </ListGroupItem>
    );
  }
}

UserItem.propTypes = {
  user: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default withRouter(UserItem);
