import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react/index';
import {
  Card,
  CardTitle,
  Progress,
  ListGroup,
  ListGroupItem,
} from 'reactstrap';
import { PAGE_TYPE } from 'constants/classificators';
import './vacationInformationWidget.css';

const titleByPage = {
  [PAGE_TYPE.USER]: (
    <span className="font-weight-600">
      Töötaja puhkuse <span className="text-primary">jäägid</span>
    </span>
  ),
  [PAGE_TYPE.DASHBOARD]: (
    <span className="font-weight-600">
      Minu puhkuse <span className="text-primary">korraldused</span>
    </span>
  ),
};

class VacationInformationWidget extends Component {
  getUserVacationProcent = (usedVacationDays = 0, holidayScheme = 0) =>
    usedVacationDays / holidayScheme * 100;

  render() {
    const { vacationData, className, pageType } = this.props;

    const spentDays = vacationData
      ? this.getUserVacationProcent(
          vacationData.holidayScheme - vacationData.usedVacationDays,
          vacationData.holidayScheme
        )
      : 0;

    return (
      <Card className={className}>
        <CardTitle className="mx-4 mb-0 mt-2 pb-2 border-bottom">
          {titleByPage[pageType]}
        </CardTitle>
        <ListGroup>
          <ListGroupItem className="border-0">
            <span className="progressTypes">Põhipuhkus</span>
            <span className="spentDays">{`${
              vacationData
                ? vacationData.holidayScheme - vacationData.usedVacationDays
                : 0
            }/${vacationData ? vacationData.holidayScheme : 0}`}</span>
            <Progress value={spentDays} className="progress" />
          </ListGroupItem>
          <ListGroupItem className="border-0">
            <span className="progressTypes">Lapsepuhkus</span>
            <span className="spentDays">0/28</span>
            <Progress color="success" value="0" className="progress" />
          </ListGroupItem>
          <ListGroupItem className="border-0">
            <span className="progressTypes">Õppepuhkus</span>
            <span className="spentDays">0/28</span>
            <Progress color="info" value={0} className="progress" />
          </ListGroupItem>
        </ListGroup>
      </Card>
    );
  }
}

VacationInformationWidget.propTypes = {
  className: PropTypes.string,
  vacationData: PropTypes.object,
  pageType: PropTypes.string.isRequired,
};

export default observer(VacationInformationWidget);
