import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { TASK_STATUS, TASK_TYPE } from 'constants/classificators';
import { parseServerDate } from 'utils/dates';

const taskNames = {
  [TASK_TYPE.START]: 'Algatatud',
  [TASK_TYPE.FINISH]: 'Kinnitamine',
  [TASK_TYPE.ACCOUNTANT]: 'Raamatupidaja kooskõlastamine',
  [TASK_TYPE.MANAGER]: 'Juhi kooskõlastamine',
};

class TimelineTask extends Component {
  render() {
    const { task } = this.props;

    const circleClass = classNames('timeline-task__circle', {
      'bg-success': task.status === TASK_STATUS.APPROVED,
      'bg-danger': task.status === TASK_STATUS.REJECTED,
      'bg-primary': task.status === TASK_STATUS.CREATED,
      'border border-primary bg-light': task.status === TASK_STATUS.NOT_CREATED,
    });

    return (
      <div className="timeline-task">
        <div className={circleClass} />
        <div className="timeline-task__description">
          <p>
            <strong>{taskNames[task.type]}</strong>{' '}
            <span className="text-muted timeline-task__data">
              Vastutav:&nbsp;
              {task.assignee
                ? `${task.assignee.firstName}\u00A0${task.assignee.lastName}`
                : ''}
              <br /> Algatatud:&nbsp;{parseServerDate(task.createdAt)}
              {(task.status === TASK_STATUS.APPROVED ||
                task.status === TASK_STATUS.REJECTED) &&
                task.updatedAt && (
                  <React.Fragment>
                    <br /> Lõpetatud:&nbsp;{parseServerDate(task.updatedAt)}
                  </React.Fragment>
                )}
            </span>
          </p>
        </div>
      </div>
    );
  }
}

TimelineTask.propTypes = {
  task: PropTypes.object.isRequired,
};

export default TimelineTask;
