import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TASK_TYPE, TASK_STATUS } from 'constants/classificators';
import TimelineTask from './TimelineTask';
import './timeline.css';

class Timeline extends Component {
  getFinishStatus = tasks => {
    if (tasks.some(t => t.status === TASK_STATUS.CREATED))
      return TASK_STATUS.NOT_CREATED;

    if (tasks.some(t => t.status === TASK_STATUS.REJECTED))
      return TASK_STATUS.NOT_CREATED;

    if (
      tasks.length === 2 &&
      tasks.every(t => t.status === TASK_STATUS.APPROVED)
    )
      return TASK_STATUS.APPROVED;
  };

  addProcessTasks = () => {
    const { vacation, process } = this.props;

    const tasks = [...process.tasks];

    const startTask = {
      type: TASK_TYPE.START,
      status: TASK_STATUS.APPROVED,
      createdAt: vacation.createdAt,
      assignee: vacation.employee,
    };

    const endTask = {
      type: TASK_TYPE.FINISH,
      status: this.getFinishStatus(process.tasks),
    };

    if (tasks.length === 1) {
      tasks.push({
        type: TASK_TYPE.ACCOUNTANT,
        status: TASK_STATUS.NOT_CREATED,
      });
    }

    return [startTask, ...tasks, endTask];
  };

  render() {
    const tasks = this.addProcessTasks();

    return (
      <div className="timeline px-5">
        {tasks.map(task => <TimelineTask task={task} key={task.type} />)}
      </div>
    );
  }
}

Timeline.propTypes = {
  process: PropTypes.object,
  vacation: PropTypes.object.isRequired,
};

export default Timeline;
