import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  ListGroup,
  ListGroupItem,
  ListGroupItemHeading,
  ListGroupItemText,
} from 'reactstrap';
import { ROLES } from 'constants/classificators';

const roleDescriptions = {
  [ROLES.EMPLOYEE]: {
    title: 'IT osakonna töötaja',
    text:
      'Antud rolliga saab puhkuse avaldusi esitada ja hallata juba loodud asju',
  },
  [ROLES.MANAGER]: {
    title: 'IT osakonna juht',
    text:
      'Antud rolliga saab töötaja poolt esitatud avaldusi vaadata, kinnitada või tagasi lükata',
  },
  [ROLES.ACCOUNTANT]: {
    title: 'Raamatupidaja',
    text:
      'Antud rolliga saab esitatud puhkuse avaldusi vaadata, kinnitada ja edastada maksmiseks ',
  },
};

class SelectActor extends Component {
  render() {
    const { onSelect, actors } = this.props;

    return (
      <ListGroup>
        {actors.map(actor => {
          const roleDescription = roleDescriptions[actor.role];
          return roleDescription ? (
            <ListGroupItem
              key={actor.id}
              id={`listActor${actor.id}`}
              action
              onClick={() => onSelect(actor.id)}
            >
              <ListGroupItemHeading>
                {roleDescription.title}
              </ListGroupItemHeading>
              <ListGroupItemText>{roleDescription.text}</ListGroupItemText>
            </ListGroupItem>
          ) : null;
        })}
      </ListGroup>
    );
  }
}

SelectActor.propTypes = {
  actors: PropTypes.object.isRequired,
  onSelect: PropTypes.func.isRequired,
};

export default SelectActor;
