import superagent from 'superagent';
import _camelCase from 'lodash/camelCase';
import _snakeCase from 'lodash/snakeCase';
import _isArray from 'lodash/isArray';
import _isObjectLike from 'lodash/isObjectLike';
import { userStore, alertStore } from 'stores';

const API_ROOT = '/api';

const handleErrors = err => {
  if (err && err.response) {
    if (err.response.status === 422) {
      const errors = formatObjectToCamelCase(err.response.body.errors);
      return Promise.reject({ validationErrors: errors });
    }
    if (err.response.status === 401) {
      userStore.logout();
      return Promise.reject(err);
    }
  }
  alertStore.addAlert('danger', 'server error');
  return Promise.reject(err);
};

const formatObjectToCamelCase = data => {
  const camelCaseObject = {};

  Object.keys(data).forEach(key => {
    const value = data[key];

    camelCaseObject[_camelCase(key)] = _isObjectLike(value)
      ? formatToCamelCase(value)
      : value;
  });

  return camelCaseObject;
};

const formatObjectToSnakeCase = data => {
  const snakeCaseObject = {};

  Object.keys(data).forEach(
    key => (snakeCaseObject[_snakeCase(key)] = data[key])
  );

  return snakeCaseObject;
};

const formatToCamelCase = data => {
  if (_isArray(data)) {
    return data.map(
      value => (_isObjectLike(value) ? formatToCamelCase(value) : value)
    );
  }

  return formatObjectToCamelCase(data);
};

const responseBody = res => {
  return res.body ? formatToCamelCase(res.body.data || res.body) : null;
};

const handleDownload = name => res => {
  const downloadElement = document.createElement('A');
  downloadElement.href = window.URL.createObjectURL(new Blob([res.body]));
  downloadElement.download = name;
  downloadElement.click();
};

const tokenPlugin = req => {
  if (userStore.user) {
    req.set('Authorization', `Bearer ${userStore.user.apiToken}`);
  }
  if (userStore.selectedActor) {
    req.set('X-Actor-Id', userStore.selectedActor);
  }
};

const getAuthHeaders = () => {
  const headers = {};

  if (userStore.user) {
    headers['Authorization'] = `Bearer ${userStore.user.apiToken}`;
  }
  if (userStore.selectedActor) {
    headers['X-Actor-Id'] = userStore.selectedActor;
  }

  return headers;
};

const requests = {
  del: url =>
    superagent
      .del(`${API_ROOT}${url}`)
      .set('Accept', 'application/json')
      .use(tokenPlugin)
      .then(responseBody),
  get: (url, query) =>
    superagent
      .get(`${API_ROOT}${url}`)
      .query(query)
      .set('Accept', 'application/json')
      .use(tokenPlugin)
      .catch(handleErrors)
      .then(responseBody),
  put: (url, body) =>
    superagent
      .put(`${API_ROOT}${url}`, body)
      .set('Accept', 'application/json')
      .use(tokenPlugin)
      .catch(handleErrors)
      .then(responseBody),
  post: (url, body) =>
    superagent
      .post(`${API_ROOT}${url}`, body)
      .set('Accept', 'application/json')
      .use(tokenPlugin)
      .catch(handleErrors)
      .then(responseBody),
  upload: (url, file) =>
    superagent
      .post(`${API_ROOT}${url}`)
      .set('Accept', 'application/json')
      .use(tokenPlugin)
      .attach('document', file)
      .catch(handleErrors)
      .then(responseBody),
  download: (url, file) =>
    superagent
      .post(`${API_ROOT}${url}`)
      .set('Accept', 'application/json')
      .use(tokenPlugin)
      .attach('document', file)
      .catch(handleErrors)
      .then(responseBody),
};

const auth = {
  login: ({ name, password }) => requests.post('/login', { name, password }),
  actors: () => requests.get('/actors'),
};

const vacation = {
  create: values =>
    requests.post('/vacations', formatObjectToSnakeCase(values)),
  update: values =>
    requests.put(`/vacations/${values.id}`, formatObjectToSnakeCase(values)),
  fetch: id => requests.get(`/vacations/${id}`),
  get: id => requests.get(`/vacations?creatorId=${id}`),
  getVacationData: id => requests.get(`/vacation_data?employeeId=${id}`),
};

const users = {
  get: () => requests.get(`/users`),
};

const process = {
  start: id => requests.post(`/vacations/${id}/processes`),
  fetch: id => requests.get(`/processes/${id}`),
  runTask: (id, status) => requests.post(`/processes/${id}/tasks`, { status }),
};

const task = {
  fetchForVacation: vacationId =>
    requests.get('/tasks', {
      vacationId,
      assigneeId: userStore.selectedActor,
    }),
  fetch: () =>
    requests.get('/tasks', {
      assigneeId: userStore.selectedActor,
    }),
};

const file = {
  url: `${API_ROOT}/files`,
  upload: file => requests.upload('/files', file),
  download: (name, fileId) =>
    superagent
      .get(`${API_ROOT}/files/${fileId}`)
      .use(tokenPlugin)
      .responseType('blob')
      .catch(handleErrors)
      .then(handleDownload(name)),
};

export default {
  auth,
  file,
  vacation,
  task,
  process,
  getAuthHeaders,
  users,
};
