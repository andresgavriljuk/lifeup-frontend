export default {
  VACATION_TYPE: 'VACATION_TYPE',
  VACATION_REASON: 'VACATION_REASON',
  VACATION_STATUS: 'VACATION_STATUS',
  ROLES: 'ROLES',
  TASK_STATUS: 'TASK_STATUS',
  TASK_TYPE: 'TASK_TYPE',
  PROCESS_STATUS: 'PROCESS_STATUS',
  PAGE_TYPE: 'PAGE_TYPE',
};

export { default as VACATION_TYPE } from './vacationType';
export { default as VACATION_REASON } from './vacationReason';
export { default as VACATION_STATUS } from './vacationStatus';
export { default as ROLES } from './roles';
export { default as TASK_STATUS } from './taskStatus';
export { default as TASK_TYPE } from './taskType';
export { default as PROCESS_STATUS } from './processStatus';
export { default as PAGE_TYPE } from './pageType';
