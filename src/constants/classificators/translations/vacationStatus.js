import VACATION_STATUS from '../vacationStatus';

export default {
  [VACATION_STATUS.DRAFT]: 'Mustand',
  [VACATION_STATUS.APPROVED]: 'Kooskõlastatud',
  [VACATION_STATUS.IN_APPROVAL]: 'Kooskõlastamisel',
  [VACATION_STATUS.REJECTED]: 'Tagasilükatud',
};
