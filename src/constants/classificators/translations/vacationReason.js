import VACATION_REASON from '../vacationReason';

export default {
  [VACATION_REASON.MARRIAGE]: 'Abiellumine',
  [VACATION_REASON.MARRIAGE_CHILD]: 'Lapse abiellumine',
  [VACATION_REASON.CLOSE_DEATH]: 'Lähedase surm',
};
