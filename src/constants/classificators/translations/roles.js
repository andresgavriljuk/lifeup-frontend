import ROLES from '../roles';

export default {
  [ROLES.EMPLOYEE]: 'Töötaja',
  [ROLES.MANAGER]: 'Juht',
  [ROLES.ACCOUNTANT]: 'Raamatupidaja',
};
