import VACATION_TYPE from '../vacationType';

export default {
  [VACATION_TYPE.FATHER]: 'Isapuhkus',
  [VACATION_TYPE.MAIN]: 'Põhipuhkus',
  [VACATION_TYPE.PARENT]: 'Vanemapuhkus',
  [VACATION_TYPE.KID]: 'Lapsepuhkus',
  [VACATION_TYPE.DISABLED_KID_PARENT]: 'Puudega lapse vanema lapsepuhkus',
  [VACATION_TYPE.UNPAID_KID]: 'Tasustamata lapsepuhkus',
  [VACATION_TYPE.UNPAID]: 'Tasustamata puhkus',
  [VACATION_TYPE.HEALTH_DAY]: 'Tervisepäev',
  [VACATION_TYPE.REDUCING_WORKING_TIME]: 'Tööajanormi vähendamine',
  [VACATION_TYPE.STUDENT]: 'Õppepuhkus',
};
