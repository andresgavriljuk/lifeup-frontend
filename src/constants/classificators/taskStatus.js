export default {
  CREATED: 'created',
  NOT_CREATED: 'notCreated',
  APPROVED: 'approved',
  REJECTED: 'rejected',
};
