export default {
  START: 'start',
  FINISH: 'finish',
  MANAGER: 'to_manager',
  ACCOUNTANT: 'to_accountant',
};
