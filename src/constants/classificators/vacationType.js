export default {
  FATHER: 'father',
  MAIN: 'main',
  PARENT: 'parent',
  KID: 'kid',
  DISABLED_KID_PARENT: 'disabled_kid_parent',
  UNPAID_KID: 'unpaid_kid',
  UNPAID: 'unpaid',
  HEALTH_DAY: 'health_day',
  REDUCING_WORKING_TIME: 'reducing_working_time',
  STUDENT: 'student',
};
