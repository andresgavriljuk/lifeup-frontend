export default {
  MARRIAGE: 'marriage',
  MARRIAGE_CHILD: 'marriageChild',
  CLOSE_DEATH: 'closeDeath',
};
