export default {
  EMPLOYEE: 'employee',
  MANAGER: 'manager',
  ACCOUNTANT: 'accountant',
};
