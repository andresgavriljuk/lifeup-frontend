export default {
  DRAFT: 'draft',
  IN_APPROVAL: 'in_approval',
  APPROVED: 'approved',
  REJECTED: 'rejected',
};
