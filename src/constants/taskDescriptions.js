import { TASK_TYPE } from 'constants/classificators';

export default {
  [TASK_TYPE.MANAGER]: {
    title: 'Juhi kooskõlastamine',
    text: 'Palun üle vaadata ja kooskõlastada puhkuse avaldus',
  },
  [TASK_TYPE.ACCOUNTANT]: {
    title: 'Raamatupidaja töölaud',
    text: 'Palun üle vaadata ja kooskõlastada puhkuse avaldus',
  },
};
