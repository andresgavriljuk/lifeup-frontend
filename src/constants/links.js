export const getVacationEditUrl = id => `/vacation/edit/${id}`;
export const getVacationViewUrl = id => `/vacation/view/${id}`;
export const getUserUrl = id => `/users/${id}`;
export const dashboardUrl = '/';
export const newVacationUrl = '/vacation/new';
