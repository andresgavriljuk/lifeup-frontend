import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { VacationForm } from 'components';

class VacationEdit extends Component {
  render() {
    return <VacationForm id={this.props.match.params.id} />;
  }
}

VacationEdit.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired,
    }),
  }),
};

export default VacationEdit;
