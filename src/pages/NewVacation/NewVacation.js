import React, { Component } from 'react';
import { VacationForm } from 'components';
import './newVacation.css';

class NewVacation extends Component {
  render() {
    return <VacationForm />;
  }
}

export default NewVacation;
