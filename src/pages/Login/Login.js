import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { LoginForm, SelectActor } from 'components';
import { observer, inject } from 'mobx-react';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';
import { dashboardUrl } from 'constants/links';
import './login.css';

class Login extends Component {
  handleSelectActor = id => {
    const { userStore, history } = this.props;

    userStore.selectActor(id);
    history.push(dashboardUrl);
  };

  render() {
    const { userStore } = this.props;

    return (
      <div className="login container-fluid">
        <div className="login__header text-center">
          <h1 className="font-weight-bold">
            Tere tulemast LifeUp äriprotsesside juhtimise demo keskkonda
          </h1>
          <p>
            Palun sisestage oma kasutajanimi ja parool keskonda sisenemiseks
          </p>
        </div>
        <LoginForm onLogin={userStore.login} />
        <Modal
          size="lg"
          isOpen={userStore.shouldSelectActor}
          centered
          toggle={userStore.logout}
          id='modalRoleSelector'
        >
          <ModalHeader toggle={userStore.logout}>Vali roll</ModalHeader>
          <ModalBody  id='modalRoleSelectorBody'>
            <SelectActor
              actors={userStore.actors}
              onSelect={this.handleSelectActor}
            />
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

Login.propTypes = {
  userStore: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default inject(stores => ({
  userStore: stores.userStore,
}))(observer(Login));
