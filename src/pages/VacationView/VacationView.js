import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { VacationView as VacationViewComponent } from 'components';

class VacationView extends Component {
  render() {
    return <VacationViewComponent id={this.props.match.params.id} />;
  }
}

VacationView.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired,
    }),
  }),
};

export default VacationView;
