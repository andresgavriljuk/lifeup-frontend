import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { inject, observer } from 'mobx-react';
import { Container, Row, Col } from 'reactstrap';
import { PAGE_TYPE, ROLES } from 'constants/classificators';
import {
  TasksWidget,
  VacationInformationWidget,
  SearchUserWidget,
} from 'components';

const titleByRole = {
  [ROLES.MANAGER]: 'Juhi töölaud',
  [ROLES.ACCOUNTANT]: 'Raamatupidaja töölaud',
};

class ManagerDashboard extends Component {
  componentWillMount() {
    const { taskStore, vacationStore, userStore } = this.props;
    taskStore.fetchUserTasks();
    vacationStore.getVacationData(userStore.actor.employeeId);
    userStore.getUsers();
  }

  componentWillReceiveProps(nextProps) {
    const { role: oldRole, taskStore, vacationStore, userStore } = this.props;
    const { role: newRole } = nextProps;

    if (newRole && oldRole !== newRole) {
      taskStore.fetchUserTasks();
      vacationStore.getVacationData(userStore.actor.employeeId);
    }
  }

  render() {
    const {
      taskStore: { userTasks },
      vacationStore: { vacationData },
      userStore: { users },
      role,
    } = this.props;

    return (
      <Container>
        <h2 className="mt-3 font-weight-600">{titleByRole[role]}</h2>
        <Row className="widgetSection">
          <Col lg="8" md="7" xs="12">
            <TasksWidget
              className="widget mt-2"
              tasks={userTasks}
              role={role}
            />
          </Col>
          <Col lg="4" md="5" xs="12">
            <VacationInformationWidget
              className="widget mt-2"
              vacationData={vacationData}
              pageType={PAGE_TYPE.DASHBOARD}
            />
          </Col>
        </Row>
        <Row className="widgetSection">
          <Col lg="8" md="7" xs="12">
            <SearchUserWidget
              className="widget mt-2"
              users={users}
              role={role}
            />
          </Col>
        </Row>
      </Container>
    );
  }
}

ManagerDashboard.propTypes = {
  taskStore: PropTypes.object.isRequired,
  vacationStore: PropTypes.object.isRequired,
  userStore: PropTypes.object.isRequired,
  role: PropTypes.string.isRequired,
};

export default inject('taskStore', 'vacationStore', 'userStore')(
  observer(ManagerDashboard)
);
