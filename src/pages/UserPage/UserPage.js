import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer, inject } from 'mobx-react';
import { UserPage as UserPageComponent } from 'components';

class UserPage extends Component {
  async componentWillMount() {
    const { userStore, vacationStore, match: { params: { id } } } = this.props;
    vacationStore.getVacations(id);
    await userStore.getUsers();
    userStore.selectEmployee(parseInt(id, 10));
    vacationStore.getVacationData(userStore.employee.userId);
  }

  render() {
    const { match: { params: { id } }, userStore } = this.props;

    const employee = userStore.employee;
    if (!employee) return null;

    return <UserPageComponent {...this.props} id={id} employee={employee} />;
  }
}

UserPage.propTypes = {
  vacationStore: PropTypes.object,
  userStore: PropTypes.object,
};

export default inject('userStore', 'vacationStore')(observer(UserPage));
