import React, { Component } from 'react';
import { VacationDashboard as VacationDashboardComponent } from 'components';

class VacationDashboard extends Component {
  render() {
    return <VacationDashboardComponent />;
  }
}

export default VacationDashboard;
