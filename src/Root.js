import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Navbar, Sidebar, PrivateRoute, Alerts } from 'components';
import {
  Login,
  NewVacation,
  VacationView,
  VacationEdit,
  VacationDashboard,
  ManagerDashboard,
  UserPage,
} from 'pages';
import { observer, inject } from 'mobx-react';
import { ROLES } from 'constants/classificators';
import './root.css';

class Root extends Component {
  render() {
    const { userStore } = this.props;

    return (
      <div>
        <Router basename="/app">
          <React.Fragment>
            <Route path="/*" component={Navbar} />
            <Alerts />

            <div className="layout">
              <Switch>
                <Route path="/login" component={Sidebar} />
                <Route path="/*" component={Sidebar} />
              </Switch>

              <div className="layout__content">
                <Route path="/login" component={Login} />
                <PrivateRoute
                  user={userStore.user}
                  actor={userStore.actor}
                  exact
                  role={userStore.actor && userStore.actor.role}
                  path="/"
                  component={
                    userStore.actor && userStore.actor.role === ROLES.EMPLOYEE
                      ? VacationDashboard
                      : ManagerDashboard
                  }
                />
                <PrivateRoute
                  user={userStore.user}
                  actor={userStore.actor}
                  path="/vacation/new"
                  component={NewVacation}
                />
                <PrivateRoute
                  user={userStore.user}
                  actor={userStore.actor}
                  path="/vacation/edit/:id"
                  component={VacationEdit}
                />
                <PrivateRoute
                  user={userStore.user}
                  actor={userStore.actor}
                  path="/vacation/view/:id"
                  component={VacationView}
                />
                <PrivateRoute
                  user={userStore.user}
                  actor={userStore.actor}
                  path="/users/:id"
                  component={UserPage}
                />
              </div>
            </div>
          </React.Fragment>
        </Router>
      </div>
    );
  }
}

Root.propTypes = {
  userStore: PropTypes.object.isRequired,
};

export default inject('userStore')(observer(Root));
