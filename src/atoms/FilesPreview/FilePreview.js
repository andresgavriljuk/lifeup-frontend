import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Modal, ModalBody, ModalHeader } from 'reactstrap';
import classNames from 'classnames';
import { Download } from 'icons';
import api from 'api';
import { Document, Page } from 'react-pdf/dist/entry.webpack';

class FilePreview extends Component {
  state = {
    fullView: false,
    numPages: null,
  };

  toggleView = () => {
    this.setState(prevState => ({ fullView: !prevState.fullView }));
  };

  handleLoad = ({ numPages }) =>
    this.setState({
      numPages,
    });

  render() {
    const { file } = this.props;
    const { fullView, numPages } = this.state;

    let viewedFile = file;

    if (file.origName) {
      viewedFile = {
        url: `${api.file.url}/${file.token}`,
        httpHeaders: api.getAuthHeaders(),
        file: { name: file.origName },
        token: file.token,
      };
    }

    const documentClass = classNames('file', { 'file--full': fullView });

    const document = (
      <div className={documentClass}>
        <Document
          file={viewedFile.url ? viewedFile : viewedFile.file}
          onLoadSuccess={this.handleLoad}
          onClick={!fullView ? this.toggleView : () => {}}
        >
          {fullView ? (
            Array.from(new Array(numPages), (el, index) => (
              <Page key={`page_${index + 1}`} pageNumber={index + 1} />
            ))
          ) : (
            <Page pageNumber={1} width={200} />
          )}
        </Document>
        {!fullView && (
          <div
            className="file__name align-middle pointer"
            onClick={() =>
              api.file.download(viewedFile.file.name, viewedFile.token)
            }
          >
            <Download /> {viewedFile.file.name}
          </div>
        )}
      </div>
    );

    return fullView ? (
      <Modal size="lg" isOpen={fullView} toggle={this.toggleView} centered>
        <ModalHeader toggle={this.toggleView}>
          {viewedFile.file.name}
        </ModalHeader>
        <ModalBody>
          <div className="preview-container">{document}</div>
        </ModalBody>
      </Modal>
    ) : (
      <div className="file">{document}</div>
    );
  }
}

FilePreview.propTypes = {
  file: PropTypes.object.isRequired,
  className: PropTypes.string,
};

export default FilePreview;
