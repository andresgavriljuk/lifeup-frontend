import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import FilePreview from './FilePreview';
import './filesPreview.css';

class FilesPreview extends Component {
  state = {
    previewVisible: false,
    viewedFile: null,
  };

  render() {
    const { files, className } = this.props;

    const uploadClass = classNames('files', className);

    return (
      <div className={uploadClass}>
        {files.map((file, index) => <FilePreview key={index} file={file} />)}
      </div>
    );
  }
}

FilesPreview.propTypes = {
  files: PropTypes.oneOfType([PropTypes.array, PropTypes.object]).isRequired,
  className: PropTypes.string,
};

export default FilesPreview;
