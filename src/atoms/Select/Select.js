import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactSelect from 'react-select';
import classNames from 'classnames';
import { Triangle } from 'icons';
import 'react-select/dist/react-select.css';
import './select.css';

class Select extends Component {
  render() {
    const { invalid } = this.props;
    const selectClass = classNames({
      'is-invalid': invalid,
    });
    return (
      <ReactSelect
        className={selectClass}
        arrowRenderer={Triangle}
        {...this.props}
      />
    );
  }
}

Select.propTypes = {
  invalid: PropTypes.bool.isRequired,
};

export default Select;
