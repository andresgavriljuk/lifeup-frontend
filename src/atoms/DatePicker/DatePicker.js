import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import ReactDatePicker from 'react-datepicker';
import classNames from 'classnames';
import { Calendar } from 'icons';
import { DATE_FORMAT, SERVER_DATE_FORMAT } from 'constants/dates';
import 'react-datepicker/dist/react-datepicker.css';
import './datePicker.css';

class DatePicker extends Component {
  handleIconClick = () => {
    this.dateInput.deferFocusInput();
  };

  render() {
    const { value, placeholder, invalid } = this.props;

    const datePickerInputClass = classNames('form-control date-picker__input', {
      'is-invalid': invalid,
    });

    const datePickerClass = classNames('date-picker', {
      'is-invalid': invalid,
    });

    const momentValue =
      !value || moment.isMoment(value)
        ? value
        : moment(value, SERVER_DATE_FORMAT);

    return (
      <div className={datePickerClass}>
        <Calendar
          className="date-picker__icon"
          onClick={this.handleIconClick}
        />
        <ReactDatePicker
          className={datePickerInputClass}
          dateFormat={DATE_FORMAT}
          selected={momentValue}
          placeholderText={placeholder}
          isClearable
          {...this.props}
          value={momentValue ? momentValue.format(DATE_FORMAT) : momentValue}
          ref={input => {
            this.dateInput = input;
          }}
        />
      </div>
    );
  }
}

DatePicker.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  invalid: PropTypes.bool.isRequired,
  placeholder: PropTypes.string,
};

DatePicker.defaultProps = {
  value: null,
  placeholder: null,
};

export default DatePicker;
