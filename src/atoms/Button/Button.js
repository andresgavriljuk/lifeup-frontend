import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button as BsButton } from 'reactstrap';
import { Loading } from 'icons';
import './button.css';

class Button extends Component {
  render() {
    const { loading, children, disabled, ...rest } = this.props;

    return (
      <BsButton {...rest} disabled={disabled || loading}>
        {loading ? <Loading className="button__loading" /> : null}
        {children}
      </BsButton>
    );
  }
}

Button.propTypes = {
  loading: PropTypes.bool,
};

Button.defaultProps = {
  loading: false,
  children: PropTypes.node,
};

export default Button;
