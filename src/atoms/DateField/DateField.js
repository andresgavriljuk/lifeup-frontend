import React, { Component } from 'react';
import PropTypes from 'prop-types';
import DatePicker from '../DatePicker/DatePicker';
import FieldWrapper from '../FieldWrapper/FieldWrapper';
import { SERVER_DATE_FORMAT } from 'constants/dates';

class DateField extends Component {
  handleChange = value => {
    const { field, form: { setFieldValue } } = this.props;

    setFieldValue(field.name, value && value.format(SERVER_DATE_FORMAT));
  };

  render() {
    return (
      <FieldWrapper
        {...this.props}
        render={props => <DatePicker {...props} onChange={this.handleChange} />}
      />
    );
  }
}

DateField.propTypes = {
  field: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,
};

export default DateField;
