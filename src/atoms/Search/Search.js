import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Input } from 'reactstrap';
import { SearchIcon } from 'icons';
import classNames from 'classnames';
import './search.css';

class Search extends Component {
  render() {
    const { className } = this.props;

    const searchClass = classNames('search', className);

    return (
      <div className={searchClass}>
        <SearchIcon className="search__icon" onClick={this.handleIconClick} />
        <Input onChange={this.props.onChange} />
      </div>
    );
  }
}

Search.propTypes = {
  className: PropTypes.string,
};

export default Search;
