import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Label, Input } from 'atoms';
import moment from 'moment';
import { classificatorAsSelect } from 'utils/classificators';
import { FormGroup } from '../FormGroup/FromGroup';
import { DATE_FORMAT, SERVER_DATE_FORMAT } from 'constants/dates';
import './staticField.css';

class StaticField extends PureComponent {
  render() {
    const { title, value, classificatorType, isDate } = this.props;

    let fieldValue = value || '-';
    if (classificatorType) {
      const classificator = classificatorAsSelect(classificatorType);
      const classificatorItem = classificator.find(
        item => item.value === value
      );

      if (classificatorItem) {
        fieldValue = classificatorItem.label;
      }
    }

    if (isDate) {
      fieldValue = value
        ? moment(value, SERVER_DATE_FORMAT).format(DATE_FORMAT)
        : value;
    }

    return (
      <FormGroup className="static-field">
        <Label>{title}</Label>
        <Input className="static-field__value" plaintext id={this.props.id}>
          {fieldValue}
        </Input>
      </FormGroup>
    );
  }
}

StaticField.propTypes = {
  value: PropTypes.string,
  title: PropTypes.string.isRequired,
  classificatorType: PropTypes.string,
  isDate: PropTypes.bool,
};

StaticField.defaultProps = {
  value: '-',
  classificatorType: null,
  isDate: false,
};

export default StaticField;
