import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Select from '../Select/Select';
import FieldWrapper from '../FieldWrapper/FieldWrapper';

class DateField extends Component {
  handleChange = value => {
    const { field, form: { setFieldValue } } = this.props;

    setFieldValue(field.name, value ? value.value : value);
  };

  render() {
    return (
      <FieldWrapper
        {...this.props}
        render={props => <Select {...props} onChange={this.handleChange} />}
      />
    );
  }
}

DateField.propTypes = {
  field: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,
};

export default DateField;
