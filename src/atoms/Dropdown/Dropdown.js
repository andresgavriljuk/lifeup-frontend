import React, { Component } from 'react';
import { Triangle } from 'icons';
import { Dropdown as BsDropdown } from 'reactstrap';
import './dropdown.css';

class Dropdown extends Component {
  render() {
    return (
      <div className="dropdown-wrapper">
        <Triangle className="dropdown-wrapper__icon" />
        <BsDropdown {...this.props} />
      </div>
    );
  }
}

export default Dropdown;
