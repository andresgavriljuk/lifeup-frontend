import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Label } from 'atoms';
import { FormGroup } from '../FormGroup/FromGroup';
import { FormFeedback, FormText } from 'reactstrap';

class FieldWrapper extends Component {
  render() {
    const {
      id,
      description,
      field,
      required,
      title,
      form: { touched, errors },
      render,
      ...rest,
    } = this.props;

    const labelClass = classNames('label text-secondary', {
      'label--required': required,
    });

    const hasError = !!touched[field.name] && !!errors[field.name];

    return (
      <FormGroup>
        <Label className={labelClass} for={id}>
          {title}
        </Label>
        {render({...rest, ...field, id, invalid: hasError})}
        {hasError && (
          <FormFeedback>{errors[field.name]}</FormFeedback>
        )}
        {description && <FormText>{description}</FormText>}
      </FormGroup>
    );
  }
}

FieldWrapper.propTypes = {
  id: PropTypes.string.isRequired,
  description: PropTypes.string,
  title: PropTypes.string.isRequired,
  required: PropTypes.bool,
  field: PropTypes.object.isRequired,
  form: PropTypes.shape({
    touched: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired,
  }),
  render: PropTypes.func.isRequired,
};

FieldWrapper.defaultProps = {
  required: false,
  description: null,
};

export default FieldWrapper;
