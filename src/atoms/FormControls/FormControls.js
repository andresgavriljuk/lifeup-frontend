import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

class FormControls extends PureComponent {
  render() {
    const { children, className } = this.props;

    const controlsClass = classNames('d-flex flex-row-reverse', className);

    return <div className={controlsClass}>{children}</div>;
  }
}

FormControls.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
};

FormControls.defaultProps = {
  className: '',
};

export default FormControls;
