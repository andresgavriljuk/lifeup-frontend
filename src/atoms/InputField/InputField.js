import React, { Component } from 'react';
import { Input } from '../Input/Input';
import FieldWrapper from '../FieldWrapper/FieldWrapper';

class InputField extends Component {
  render() {
    return (
      <FieldWrapper {...this.props} render={props => <Input {...props} />} />
    );
  }
}

export default InputField;
