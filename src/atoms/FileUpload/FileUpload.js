import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Dropzone from 'react-dropzone';
import { Upload } from 'icons';
import './fileUpload.css';

class FileUpload extends Component {
  render() {
    const { onDrop } = this.props;

    return (
      <Dropzone
        onDrop={onDrop}
        accept="application/pdf"
        className="file-upload"
        acceptClassName="border-success"
        rejectClassName="border-danger"
        multiple={false}
      >
        <Upload />
        <span className="file-upload__text ml-2">
          Valige arvutist fail või lohistage see siia
        </span>
      </Dropzone>
    );
  }
}

FileUpload.propTypes = {
  onDrop: PropTypes.func.isRequired,
};

export default FileUpload;
