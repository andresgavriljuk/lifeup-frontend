import { decorate, observable, action } from 'mobx';
import { alertStore } from 'stores';
import ALERT_TYPE from 'constants/alertType';
import api from 'api';

class VacationStore {
  vacation = {};
  vacations = [];
  vacationData = {};

  async createVacation(values) {
    try {
      this.vacation = await api.vacation.create(values);

      alertStore.addAlert(ALERT_TYPE.SUCCESS, 'Puhkuseavaldus edukalt loodud');

      return this.vacation.id;
    } catch (errors) {
      return { errors };
    }
  }

  async updateVacation(values) {
    try {
      this.vacation = await api.vacation.update(values);

      alertStore.addAlert(
        ALERT_TYPE.SUCCESS,
        'Muudatused on edukalt salvestatud!'
      );

      return this.vacation.id;
    } catch (errors) {
      return errors;
    }
  }

  async getVacations(id) {
    this.vacations = await api.vacation.get(id);
  }

  async getVacationData(id) {
    this.vacationData = await api.vacation.getVacationData(id);
  }

  async fetchVacation(id) {
    this.vacation = await api.vacation.fetch(id);
  }

  async uploadFile(file) {
    return await api.file.upload(file);
  }

  resetVacation() {
    this.vacation = {};
  }
}

const VacationStoreMobx = decorate(VacationStore, {
  vacation: observable,
  vacations: observable,
  vacationData: observable,
  fetchVacation: action.bound,
  createVacation: action.bound,
  resetVacation: action.bound,
});

export default new VacationStoreMobx();
