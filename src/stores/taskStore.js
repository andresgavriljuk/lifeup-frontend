import { decorate, observable, action } from 'mobx';

import api from 'api';

class TaskStore {
  vacationTasks = [];
  userTasks = [];

  async fetchVacationTasks(vacationId) {
    this.vacationTasks = await api.task.fetchForVacation(vacationId);
  }

  async fetchUserTasks() {
    this.userTasks = await api.task.fetch();
  }
}

const TaskStoreMobx = decorate(TaskStore, {
  vacationTasks: observable,
  userTasks: observable,
  fetchVacationTasks: action.bound,
  fetchUserTasks: action.bound,
});

export default new TaskStoreMobx();
