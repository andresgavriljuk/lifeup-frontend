import { decorate, observable, action } from 'mobx';
import { v4 } from 'uuid';

const ALERT_DURATION = 5000;

class AlertStore {
  alerts = [];

  addAlert(type, message) {
    const alert = { id: v4(), type, message };

    this.alerts.push(alert);

    setTimeout(() => {
      this.removeAlert(this.alerts[this.alerts.length - 1]);
    }, ALERT_DURATION);
  }

  removeAlert(alert) {
    this.alerts.remove(alert);
  }
}

const AlertStoreMobx = decorate(AlertStore, {
  alerts: observable,
  addAlert: action.bound,
  removeAlert: action.bound,
});

export default new AlertStoreMobx();
