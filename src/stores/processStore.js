import { decorate, observable, action } from 'mobx';
import { alertStore, vacationStore, taskStore } from 'stores';
import { TASK_STATUS } from 'constants/classificators';
import ALERT_TYPE from 'constants/alertType';

import api from 'api';

class ProcessStore {
  process = {};

  async startProcess(id) {
    this.process = await api.process.start(id);
    alertStore.addAlert(ALERT_TYPE.SUCCESS, 'Protsess edukalt algatatud');

    vacationStore.fetchVacation(id);
  }

  async fetchProcess(id) {
    this.process = await api.process.fetch(id);
  }

  async runTask(status, processId, vacationId) {
    this.process = await api.process.runTask(processId, status).then(() => {
      alertStore.addAlert(
        ALERT_TYPE.SUCCESS,
        status === TASK_STATUS.APPROVED
          ? 'Dokument on kooskõlastatud'
          : 'Dokument on tagasilükkatud'
      );

      vacationStore.fetchVacation(vacationId);
      taskStore.fetchTasks(vacationId);
    });
  }
}

const ProcessStoreMobx = decorate(ProcessStore, {
  process: observable,
  startProcess: action.bound,
  runTask: action.bound,
});

export default new ProcessStoreMobx();
