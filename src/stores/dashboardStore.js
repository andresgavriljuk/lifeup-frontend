import { decorate, observable, action } from 'mobx';

class DashboardStore {
  newProcessModalVisible = false;

  toggleNewProcess() {
    this.newProcessModalVisible = !this.newProcessModalVisible;
  }
}

const DashboardStoreMobx = decorate(DashboardStore, {
  newProcessModalVisible: observable,
  toggleNewProcess: action.bound,
});

export default new DashboardStoreMobx();
