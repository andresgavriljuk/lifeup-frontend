import { decorate, observable, action, toJS, computed } from 'mobx';
import api from 'api';

const STORE_KEY = 'lifeup-bs_store';

class UserStore {
  constructor({ user, actors = [], selectedActor }) {
    this.user = user;
    this.actors = actors;
    this.selectedActor = selectedActor;
  }

  user = undefined;
  users = [];
  employee = undefined;

  get actor() {
    return this.actors.find(actor => actor.id === this.selectedActor);
  }

  get shouldSelectActor() {
    return this.user && this.actors.length > 0 && !this.selectedActor;
  }

  async login(name, password) {
    try {
      this.user = await api.auth.login(name, password);

      this.saveToLocalStorage();
      this.loadActors();
    } catch (errors) {
      return { errors };
    }
  }

  logout() {
    this.user = undefined;
    this.actors = [];
    this.selectedActor = undefined;

    this.saveToLocalStorage();
  }

  async loadActors() {
    this.actors = await api.auth.actors();

    this.saveToLocalStorage();
  }

  selectEmployee(id) {
    this.employee = this.users.find(user => user.id === id);
  }

  selectActor(actorId) {
    this.selectedActor = actorId;
    this.saveToLocalStorage();
  }

  saveToLocalStorage() {
    localStorage.setItem(STORE_KEY, JSON.stringify(toJS(this)));
  }

  async getUsers() {
    this.users = await api.users.get();
  }
}

const UserStoreMobx = decorate(UserStore, {
  actors: observable,
  employee: observable,
  selectedActor: observable,
  users: observable,
  actor: computed,
  shouldSelectActor: computed,
  user: observable,
  login: action.bound,
  logout: action.bound,
  selectActor: action.bound,
});

export default new UserStoreMobx(JSON.parse(localStorage[STORE_KEY] || '{}'));
