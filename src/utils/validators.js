export const parseServerValidation = errors => {
  const formikErrors = {};

  Object.keys(errors).forEach(key => (formikErrors[key] = errors[key][0]));

  return formikErrors;
};
