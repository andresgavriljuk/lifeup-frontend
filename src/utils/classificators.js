import * as classificators from 'constants/classificators';
import * as translations from 'constants/classificators/translations';

export const classificatorAsSelect = type =>
  Object.values(classificators[type]).map(key => ({
    value: key,
    label: translations[type][key],
  }));

export const translateClassificatorValue = (type, value) =>
  translations[type][value];
