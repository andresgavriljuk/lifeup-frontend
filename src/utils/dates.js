import moment from 'moment';
import { DATE_FORMAT, SERVER_DATE_FORMAT } from 'constants/dates';

export const parseServerDate = value =>
  value ? moment(value, SERVER_DATE_FORMAT).format(DATE_FORMAT) : value;

const dateSorterDesc = field => (a, b) =>
  new Date(b[field]) - new Date(a[field]);

export const vacationDateSorter = dateSorterDesc('createdAt');
export const taskDateSorter = dateSorterDesc('createdAt');
