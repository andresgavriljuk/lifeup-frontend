import { setLocale } from 'yup/lib/customLocale';
import yup from 'yup';
import moment from 'moment';

setLocale({
  mixed: {
    required: 'Kohustuslik väli',
  },
});

yup.addMethod(yup.date, 'notFr', function(message) {
  return this.test('notFr', message, function(value) {
    return ![5].includes(moment(value).day());
  });
});

yup.addMethod(yup.date, 'isAfter', function(ref, message) {
  return this.test('isAfter', message, function(value) {
    let other = this.resolve(ref);

    return !other || !value || moment(value).isAfter(other);
  });
});
