import React from 'react';

export default () => (
  <svg width="24" height="23" viewBox="0 0 24 23">
    <g
      fill="none"
      fillRule="evenodd"
      stroke="#4285F4"
      strokeLinecap="round"
      strokeLinejoin="round"
    >
      <path d="M3.5 12.5v10h6v-7h5v7h6V13M.5 12L12 .5 23.5 12M16 1.5h3.5V5" />
    </g>
  </svg>
);
