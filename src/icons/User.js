import React from 'react';

export default () => (
  <svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 36 36">
    <g fill="none" fillRule="evenodd">
      <circle cx="18" cy="18" r="18" fill="#CFD8DC"/>
      <path stroke="#FFF" strokeLinejoin="round" d="M17.5 28H29s0-2.057-1-4.057c-.746-1.491-4-2.5-8-4v-3s1.5-1 1.5-3c.5 0 1-2 0-2.5 0-.297 1.339-2.801 1-4.5-.5-2.5-7.5-2.5-8-.5-3 0-1 4.594-1 5-1 .5-.5 2.5 0 2.5 0 2 1.5 3 1.5 3v3c-4 1.5-7.255 2.509-8 4C6 25.943 6 28 6 28h11.5z"/>
    </g>
  </svg>
);
