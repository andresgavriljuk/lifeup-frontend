import React from 'react';

export default props => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="26"
    height="26"
    viewBox="0 0 26 26"
    {...props}
  >
    <g
      fill="none"
      fillRule="evenodd"
      stroke="#01579B"
      strokeLinejoin="round"
      strokeWidth="2"
    >
      <path d="M17.5 9.5a8.001 8.001 0 0 1-16 0 8 8 0 0 1 16 0z" />
      <path strokeLinecap="round" d="M15.156 15.156L24.5 24.5" />
    </g>
  </svg>
);
