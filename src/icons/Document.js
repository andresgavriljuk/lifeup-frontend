import React from 'react';

export default props => (
  <svg width="23" height="19" viewBox="0 0 23 19" {...props}>
    <path
      fill="none"
      fillRule="evenodd"
      stroke="#01579B"
      strokeLinecap="round"
      strokeLinejoin="round"
      d="M3.625 18.063h15.313a2.627 2.627 0 0 0 2.625-2.625v-10.5H5.813v10.937a2.188 2.188 0 0 1-4.375 0V1.437h6.125v1.75h11.375v1.75"
    />
  </svg>
);
