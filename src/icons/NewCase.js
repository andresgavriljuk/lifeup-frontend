import React from 'react';

export default props => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
    width="42"
    height="42"
    viewBox="0 0 42 42"
    {...props}
  >
    <defs>
      <circle id="b" cx="17" cy="17" r="17" />
      <filter
        id="a"
        width="141.2%"
        height="141.2%"
        x="-20.6%"
        y="-14.7%"
        filterUnits="objectBoundingBox"
      >
        <feoffset dy="2" in="SourceAlpha" result="shadowOffsetOuter1">
          <fegaussianblur
            in="shadowOffsetOuter1"
            result="shadowBlurOuter1"
            stdDeviation="2"
          >
            <fecolormatrix
              in="shadowBlurOuter1"
              values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.2 0"
            />
          </fegaussianblur>
        </feoffset>
      </filter>
    </defs>
    <g fill="none" fillRule="evenodd">
      <g transform="translate(4 2)">
        <use fill="#000" filter="url(#a)" xlinkHref="#b" />
        <use fill="#4285F4" xlinkHref="#b" />
      </g>
      <path
        stroke="#FFF"
        strokeLinecap="round"
        strokeLinejoin="round"
        d="M27.188 12.938v-1.75H15.813v-1.75h-4.376V23a1.312 1.312 0 1 0 2.626 0V12.937h14.874v3.938M12.75 24.313h6.125"
      />
      <path
        stroke="#FFF"
        strokeLinejoin="round"
        d="M31.563 24.313a5.25 5.25 0 1 1-10.5 0 5.25 5.25 0 0 1 10.5 0z"
      />
      <path
        stroke="#FFF"
        strokeLinecap="round"
        strokeLinejoin="round"
        d="M23.926 24.313h4.773M26.313 26.7v-4.774"
      />
    </g>
  </svg>
);
