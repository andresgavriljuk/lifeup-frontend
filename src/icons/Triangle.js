import React from 'react';

export default props => (
  <svg width="14" height="11" viewBox="0 0 14 11" className={props.className}>
    <path
      fill="none"
      fillRule="evenodd"
      stroke="#4285F4"
      d="M6.26 9.83L1.18 2.573A1 1 0 0 1 2 1H12.16a1 1 0 0 1 .819 1.573L7.898 9.83a1 1 0 0 1-1.638 0z"
    />
  </svg>
);
