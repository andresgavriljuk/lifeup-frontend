import React from 'react';

export default props => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="20"
    height="20"
    viewBox="0 0 33 33"
    {...props}
  >
    <g
      fill="none"
      fillRule="evenodd"
      stroke="#01579B"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth="2"
    >
      <path d="M32 16.5C32 25.059 25.06 32 16.5 32 7.94 32 1 25.059 1 16.5 1 7.938 7.94 1 16.5 1 25.06 1 32 7.938 32 16.5zM16 8v17M25 16H8" />
    </g>
  </svg>
);
