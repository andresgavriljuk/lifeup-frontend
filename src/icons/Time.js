import React from 'react';

export default props => (
  <svg width="22" height="22" viewBox="0 0 22 22" {...props}>
    <g
      fill="none"
      fillRule="evenodd"
      stroke="#CFD8DC"
      strokeLinecap="round"
      strokeLinejoin="round"
    >
      <path d="M20.583 11c0 5.293-4.291 9.583-9.583 9.583A9.581 9.581 0 0 1 1.417 11 9.581 9.581 0 0 1 11 1.417c5.292 0 9.583 4.29 9.583 9.583z" />
      <path d="M10.583 6.417V11l5 4.583" />
    </g>
  </svg>
);
