import React from 'react';

export default props => (
  <svg width="24" height="24" viewBox="0 0 24 24" {...props}>
    <g
      fill="none"
      fillRule="evenodd"
      stroke="#4285F4"
      strokeLinecap="round"
      strokeLinejoin="round"
    >
      <path d="M4.5 2.5h-4v21h23v-21h-4" />
      <path d="M7.5 4.5h-3v-4h3zM19.5 4.5h-3v-4h3zM7.5 2.5h9M.5 7.5h23M6.5 9.5v12M11.5 9.5v12M16.5 9.5v12M2.5 11.5h19M2.5 15.5h19M2.5 19.5h19" />
    </g>
  </svg>
);
