import React, { Component } from 'react';
import Root from './Root';
import { Provider } from 'mobx-react';
import * as stores from 'stores';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Provider {...stores}>
          <Root />
        </Provider>
      </div>
    );
  }
}

export default App;
