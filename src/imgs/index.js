import avatar from './avatar.png';
import UserAvatar from './userAvatar.png';
import UnknownAvatar from './unknownAvatar.png';

export { avatar, UserAvatar, UnknownAvatar };
